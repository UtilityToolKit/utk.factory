// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/container_policy/StdMapCompatibleContainerPolicy.hpp
//
// Description: StdMapCompatibleContainerPolicy class definition.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_CONTAINER_POLICY_STDMAPCOMPATIBLECONTAINERPOLICY_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_CONTAINER_POLICY_STDMAPCOMPATIBLECONTAINERPOLICY_HPP


#include <type_traits>
#include <utility>

#include "utk/factory/v2/namespace.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief A container policy for using std::map-compatible containers with
   ObjectRegistry

   @details The following member functions are required to be provided by the
   container type:

    * emplace()

    * erase()

    * find()

   @tparam StdMapLike_ The type of the container compatible with std::map. If
   the container type has more then just 2 template parameters, then a partial
   specialisation of the type should be provided.

   @tparap ContainerInitializer_ The type providing a function for container
   initialisation.
*/
template <
    template < class Key_, class Value_ > class StdMapLike_,
    template < class ContainerPolicy_ > class ContainerInitializer_ >
class StdMapCompatibleContainerPolicy {
public:
	/**
	   @brief Type of the policy

	   @details For simplifying mentioning type in generic code.
	 */
	using SelfType =
	    StdMapCompatibleContainerPolicy< StdMapLike_, ContainerInitializer_ >;

	/**
	   @brief Container type

	   @details The type of the container compatible with std::map

	   @tparam Key_ The type of the keys of the map

	   @tparam Value_ The type of the values stored in the map

	   @tparam Others_ Other, container type specific, template arguments.
	   Required to handle container types with defaulted template parameters.
	*/
	template < class Key_, class Value_ >
	using Container = StdMapLike_< Key_, Value_ >;

	/**
	   @brief Container initialiser type

	   @details Provides InitContainer() static member function for default
	   container initialisation.
	*/
	using ContainerInitializer = ContainerInitializer_< SelfType >;

	/**
	   @brief Provides access to container Key_ type
	*/
	template < class Container_ >
	using Key = typename ::std::remove_reference_t< Container_ >::key_type;

	/**
	   @brief Provides access to container Value_ type
	*/
	template < class Container_ >
	using Value = typename ::std::remove_reference_t< Container_ >::mapped_type;


	/**
	   @brief Initialises the given container

	   @details Calls ContainerInitializer::InitContainer().

	   @tparam Container_ The type of the container to initialise.

	   @param [in,out] io_container The container to initialise.
	*/
	template < class Container_ >
	static void InitContainer (Container_&& io_container)
	{

		ContainerInitializer::InitContainer (
		    ::std::forward< Container_ > (io_container));
	}


	/**
	   @brief Puts the value with the given key into the container if there is
	   no other value with the save key in the container already

	   @tparam Key_ The type of the keys of the object to add to the container.

	   @tparam Value_ The type of the object to add to the container.

	   @tparam Container_ The type of the container to add value into.

	   @param [in,out] io_container The he container to add value into.

	   @param [in] i_key The keys of the object to add to the container.

	   @param [in] i_value The object to add to the container.

	   @returns "true" on success, "false" otherwise.
	*/
	template < class Key_, class Value_, class Container_ >
	static bool
	    Emplace (Container_&& io_container, Key_&& i_key, Value_&& i_value)
	{
		auto const emplace_result = ::std::forward< Container_ > (io_container)
		                                .emplace (
		                                    ::std::forward< Key_ > (i_key),
		                                    ::std::forward< Value_ > (i_value));

		return emplace_result.second;
	}


	/**
	   @brief Puts the value with the given key into the container or replaces
	   the value with the same key if any is already is in container

	   @tparam Key_ The type of the keys of the object to add to the container.

	   @tparam Value_ The type of the object to add to the container.

	   @tparam Container_ The type of the container to add value into.

	   @param [in,out] io_container The he container to add value into.

	   @param [in] i_key The keys of the object to add to the container.

	   @param [in] i_value The object to add to the container.
	*/
	template < class Key_, class Value_, class Container_ >
	static void
	    AddOrReplace (Container_&& io_container, Key_&& i_key, Value_&& i_value)
	{
		auto const emplace_result =
		    ::std::forward< Container_ > (io_container)
		        .emplace (::std::forward< Key_ > (i_key), i_value);

		if (!emplace_result.second)
		{
			emplace_result.first->second = ::std::forward< Value_ > (i_value);
		}
	}


	/**
	   @brief Removes the value with the given key from the container

	   @tparam Key_ The type of the keys of the object to add to the container.

	   @tparam Container_ The type of the container to add value into.

	   @param [in,out] io_container The he container to add value into.

	   @param [in] i_key The keys of the object to add to the container.
	*/
	template < class Key_, class Container_ >
	static void Erase (Container_&& io_container, Key_&& i_key)
	{
		::std::forward< Container_ > (io_container)
		    .erase (::std::forward< Key_ > (i_key));
	}


	/**
	   @brief Requests an iterator to the object with the given key

	   @tparam Key_ The type of the keys of the object to add to the container.

	   @tparam Container_ The type of the container to add value into.

	   @param [in] i_container The he container to add value into.

	   @param [in] i_key The keys of the object to add to the container.

	   @returns An iterator pointing to the pair containing the requested value
	   or std::end(io_container) if the requested value was not found.
	*/
	template < class Key_, class Container_ >
	static auto Find (Container_&& i_container, Key_&& i_key)
	{
		return ::std::forward< Container_ > (i_container)
		    .find (::std::forward< Key_ > (i_key));
	}


	/**
	   @brief Checks if an iterator is valid

	   @tparam Container_ The type of the container to add value into.

	   @tparam Iterator_ The type of the iterator pointing to the object.

	   @param [in] i_container The he container to add value into.

	   @param [in] i_iterator The iterator pointing to the object.

	   @returns "true" if an iterator is valid (i.e. points to some value inside
	   the container) or "false" otherwise.
	*/
	template < class Container_, class Iterator_ >
	static bool
	    IsIteratorValid (Container_&& i_container, Iterator_&& i_iterator)
	{
		CheckIteratorType< Container_, Iterator_ > ();

		return ::std::end (::std::forward< Container_ > (i_container)) !=
		    ::std::forward< Iterator_ > (i_iterator);
	}


	/**
	   @brief Dereferences the given iterator

	   @tparam Container_ The type of the container to add value into.

	   @tparam Iterator_ The type of the iterator pointing to the object.

	   @param [in] i_container The he container to add value into.

	   @param [in] i_iterator The iterator pointing to the object.

	   @returns A reference to the value pointer by the given iterator, i.e.
	   Value& for Container< Key, Value >.

	   @note The behaviour is undefined if the given iterator is invalid.
	*/
	template < class Container_, class Iterator_ >
	static auto&
	    IteratorValue (Container_&& /*i_container*/, Iterator_&& i_iterator)
	{
		CheckIteratorType< Container_, Iterator_ > ();

		return ::std::forward< Iterator_ > (i_iterator)->second;
	}


private:
	template < class Container_, class Iterator_ >
	constexpr static void CheckIteratorType ()
	{
		using ContainerType = ::std::remove_reference_t< Container_ >;
		using IteratorType =
		    ::std::remove_const_t< ::std::remove_reference_t< Iterator_ > >;

		static_assert (
		    (::std::is_same< IteratorType, typename ContainerType::iterator >::
		         value ||
		     ::std::is_same<
		         IteratorType,
		         typename ContainerType::const_iterator >::value ||
		     ::std::is_same<
		         IteratorType,
		         typename ContainerType::reverse_iterator >::value ||
		     ::std::is_same<
		         IteratorType,
		         typename ContainerType::const_reverse_iterator >::value),
		    "Invalid iterator type");
	};
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif // UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_CONTAINER_POLICY_STDMAPCOMPATIBLECONTAINERPOLICY_HPP
