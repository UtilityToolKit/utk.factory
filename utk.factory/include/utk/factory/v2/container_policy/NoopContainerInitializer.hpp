// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/container_policy/NoopContainerInitializer.hpp
//
// Description: NoopContainerInitializer class definition.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_CONTAINER_POLICY_NOOPCONTAINERINITIALIZER_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_CONTAINER_POLICY_NOOPCONTAINERINITIALIZER_HPP


#include "utk/factory/v2/namespace.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief A container initialiser that does nothing

   @tparam ContainerPolicy_ The container policy providing container for
   initialisation.
*/
template < class ContainerPolicy_ >
struct NoopContainerInitializer {
	/**
	   @brief Does nothing

	   @tparam Container_ Type of the container for initialisation.
	*/
	template < class Container_ >
	static void InitContainer (Container_&&)
	{
	}
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif // UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_CONTAINER_POLICY_NOOPCONTAINERINITIALIZER_HPP
