// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/optional_policy/BoostOptionalPolicy.hpp
//
// Description: BoostOptionalPolicy class definition.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_OPTIONAL_POLICY_BOOSTOPTIONALPOLICY_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_OPTIONAL_POLICY_BOOSTOPTIONALPOLICY_HPP


#include <type_traits>

#include <boost/optional.hpp>

#include "utk/factory/v2/namespace.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief An optional type policy for using boost::optional as optional value
   type

   @note Users are not forced to use access functions provided by the policy to
   interact with the optional values in non-generic code.
*/
class BoostOptionalPolicy {
private:
	template < class T_ >
	using ClearType = ::std::remove_cv_t< ::std::remove_reference_t< T_ > >;


public:
	/**
	   @brief Optional value type

	   @tparam Value_ The type of the optional value
	*/
	template < class Value_ >
	using OptionalType = ::boost::optional< Value_ >;

	/**
	   @brief Optional reference type

	   @tparam Value_ The type of the value optional reference is referencing.
	*/
	template < class Value_ >
	using OptionalRefType = ::boost::optional< Value_& >;

	/**
	   @brief Optional const reference type

	   @tparam Value_ The type of the value optional const reference is
	   referencing.
	*/
	template < class Value_ >
	using OptionalConstRefType = ::boost::optional< Value_ const& >;


	/**
	   @brief Constructs an empty (invalid) optional object

	   @tparam Value_ The type of the value that could have been stored in the
	   optional object if it was not empty

	   @returns An empty (invalid) optional object.
	*/
	template < class Value_ >
	static OptionalType< Value_ > NullOptional ()
	{
		return ::boost::none;
	}


	/**
	   @brief Gets a reference to the value or the reference stored in the given
	   optional value object

	   @tparam Value_ The type of the value stored in the optional object

	   @tparam Optional_ The type of the optional object

	   @param [in] i_optional Optional object holding the value of interest.

	   @returns A reference to the value stored in the given optional object.

	   @note If i_optional does not contain a value then the behaviour is
	   undefined.
	*/
	template < class Value_, template < class ValueType_ > class Optional_ >
	static auto& Get (Optional_< Value_ > const& i_optional)
	{
		CheckOptionalType< Optional_, Value_ > ();

		return *i_optional;
	}


	/**
	   @brief Checks if optional object is holding any value

	   @tparam Value_ The type of the value stored in the optional object

	   @tparam Optional_ The type of the optional object

	   @param [in] i_optional Optional object to check.

	   @returns "true" if the optional object is holding a value, "false"
	   otherwise.
	*/
	template < class Value_, template < class ValueType_ > class Optional_ >
	static bool HasValue (Optional_< Value_ > const& i_optional)
	{
		CheckOptionalType< Optional_, Value_ > ();

		return bool(i_optional);
	}


	/**
	   @brief Creates an optional object holding the given value

	   @tparam Value_ The type of the value for storing in the optional object.

	   @param [in] i_value Value to store in the created optional object.

	   @returns An optional object holding the given value.
	*/
	template < class Value_ >
	static OptionalType< ClearType< Value_ > > MakeOptional (Value_&& i_value)
	{
		return ::std::forward< Value_ > (i_value);
	}


	/**
	   @brief Creates an optional object from the provided arguments

	   @tparam Value_ The type of the value for storing in the optional object.

	   @trapam Args_ Parameter pack of types of arguments for constructing the
	   value.

	   @param [in] i_args Arguments constructing the value.

	   @returns An optional object holding the given value.
	*/
	template < class Value_, class... Args_ >
	static OptionalType< Value_ > MakeOptional (Args_&&... i_args)
	{
		return Value_{::std::forward< Args_ > (i_args)...};
	}


	/**
	   @brief Creates an optional object holding the given value

	   @tparam Value_ The type of the value a reference to which will be
	   stored the in optional object.

	   @param [in] i_value Value to store reference to it in the created
	   optional object.

	   @returns An optional object holding a reference to the given value.
	*/
	template < class Value_ >
	static OptionalRefType< ClearType< Value_ > >
	    MakeOptionalRef (Value_&& i_value)
	{
		static_assert (
		    !::std::is_rvalue_reference< Value_ >::value,
		    "Referencing rvalue is prohibited");

		return ::std::forward< Value_ > (i_value);
	}


	/**
	   @brief Creates an optional object holding the given value

	   @tparam Value_ The type of the value a const reference to which will
	   be stored in the optional object.

	   @param [in] i_value Value to store const reference to it in the
	   created optional object.

	   @returns An optional object holding a const reference to the given
	   value.
	*/
	template < class Value_ >
	static OptionalConstRefType< ClearType< Value_ > >
	    MakeOptionalConstRef (Value_&& i_value)
	{
		static_assert (
		    !::std::is_rvalue_reference< Value_ >::value,
		    "Referencing rvalue is prohibited");

		return ::std::forward< Value_ > (i_value);
	}


private:
	template < template < class ValueType_ > class Optional_, class Value_ >
	constexpr static void CheckOptionalType ()
	{
		static_assert (
		    (::std::is_same< OptionalType< Value_ >, Optional_< Value_ > >::
		         value ||
		     ::std::is_same< OptionalRefType< Value_ >, Optional_< Value_ > >::
		         value ||
		     ::std::is_same<
		         OptionalConstRefType< Value_ >,
		         Optional_< Value_ > >::value),
		    "Argument type is incompatible with optional policy.");
	};
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif // UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_OPTIONAL_POLICY_BOOSTOPTIONALPOLICY_HPP
