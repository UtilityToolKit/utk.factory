// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/optional_policy/SmartPointerAsOptionalPolicy.hpp
//
// Description: SmartPointerAsOptionalPolicy class definition.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_OPTIONAL_POLICY_SMARTPOINTERASOPTIONALPOLICY_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_OPTIONAL_POLICY_SMARTPOINTERASOPTIONALPOLICY_HPP


#include <type_traits>

#include "utk/factory/v2/namespace.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief An optional type policy for using smart pointers as optional value
   type

   @details Different smart pointer policies allow for using payload-specific
   smart pointers with appropriate allocators and deleters.

   @tparam ReferencePolicy_ The policy describing how references are stored in
   optional objects.

   @tparam ValuePointerPolicy_ The policy describing how optional values are
   stored in smart pointer objects.

   @tparam RefPointerPolicy_ The policy describing how optional references are
   stored in smart pointer objects.

   @tparam ConstRefPointerPolicy_ The policy describing how optional const
   references are stored in smart pointer objects.

   @note Users are not forced to use access functions provided by the policy to
   interact with the optional values in non-generic code.
*/
template <
    class ReferencePolicy_,
    class ValuePointerPolicy_,
    class RefPointerPolicy_ = ValuePointerPolicy_,
    class ConstRefPointerPolicy_ = ValuePointerPolicy_ >
class SmartPointerAsOptionalPolicy {
public:
	/**
	   @brief The policy describing how references are stored in optional
	   objects

	   @details Provides a type for storing references and functions for
	   manipulating objects holding references:

	    * Get() - returns the stored reference

	    * MakeRef() - creates an object holding a reference to the given value

	    * MakeConstRef() - creates an object holding a const reference to the
	    given value
	*/
	using ReferencePolicy = ReferencePolicy_;

	/**
	   @brief The type for storing references

	   @tparam T_ The type of the value a reference to which is stored in
	   reference holding object.
	*/
	template < class T_ >
	using Reference = typename ReferencePolicy::template Reference< T_ >;

	/**
	   @brief The policy describing how optional values are stored in smart
	   pointer objects

	   @details Provides smart pointer type and the MakeSmartPointer() function
	   for effective construction of smart pointer objects.
	*/
	using ValuePointerPolicy = ValuePointerPolicy_;

	/**
	   @brief The policy describing how optional references are stored in smart
	   pointer objects

	   @details @see ValuePointerPolicy
	*/
	using RefPointerPolicy = RefPointerPolicy_;

	/**
	   @brief The policy describing how optional const references are stored in
	   smart pointer objects

	   @details @see ValuePointerPolicy
	*/
	using ConstRefPointerPolicy = ConstRefPointerPolicy_;

	/**
	   @brief Optional value type

	   @tparam Value_ The type of the optional value
	*/
	template < class Value_ >
	using OptionalType =
	    typename ValuePointerPolicy::template SmartPointer< Value_ >;

	/**
	   @brief Optional reference type

	   @tparam Value_ The type of the value optional reference is referencing.
	*/
	template < class Value_ >
	using OptionalRefType =
	    typename RefPointerPolicy::template SmartPointer< Reference< Value_ > >;

	/**
	   @brief Optional const reference type

	   @tparam Value_ The type of the value optional const reference is
	   referencing.
	*/
	template < class Value_ >
	using OptionalConstRefType =
	    typename ConstRefPointerPolicy::template SmartPointer<
	        Reference< Value_ const > >;


private:
	template <
	    template < class V_, class... Os_ > class Optional_,
	    class Value_,
	    class... Others_ >
	struct IsOptionalValueType;

	template <
	    template < class V_, class... Os_ > class Optional_,
	    class Value_,
	    class... Others_ >
	struct IsOptionalRefType;

	template < class Value_ >
	struct OptionalTypeForValue;


public:
	/**
	   @brief Constructs an empty (invalid) optional object

	   @tparam Value_ The type of the value that could have been stored in the
	   optional object if it was not empty

	   @returns An empty (invalid) optional object.
	*/
	template < class Value_ >
	static auto NullOptional () ->
	    typename OptionalTypeForValue< Value_ >::Optional
	{
		return nullptr;
	}


	/**
	   @brief Gets a reference to the value stored in the given optional value
	   object

	   @tparam Value_ The type of the value stored in the optional object

	   @tparam Others_ Parameters pack for handling template parameters of smart
	   pointer type that are out of scope of this optional policy.

	   @tparam Optional_ The type of the optional object

	   @param [in] i_optional Optional object holding the value of interest.

	   @returns A reference to the value stored in the given optional object.

	   @note If i_optional does not contain a value then the behaviour is
	   undefined.
	*/
	template <
	    class Value_,
	    class... Others_,
	    template < class V_, class... Os_ > class Optional_ >
	static auto Get (Optional_< Value_, Others_... > const& i_optional)
	    -> ::std::enable_if_t<
	        IsOptionalValueType< Optional_, Value_, Others_... >::kValue,
	        Value_& >
	{
		return *i_optional;
	}


	/**
	   @brief Gets the reference stored in optional reference object

	   @tparam Value_ The type of the value stored in the optional object

	   @tparam Others_ Parameters pack for handling template parameters of smart
	   pointer type that are out of scope of this optional policy.

	   @tparam Optional_ The type of the optional object

	   @param [in] i_optional Optional object holding the value of interest.

	   @returns A reference to the value stored in the given optional object.

	   @note If i_optional does not contain a value then the behaviour is
	   undefined.
	*/
	template <
	    class Value_,
	    class... Others_,
	    template < class V_, class... Os_ > class Optional_ >
	static auto
	    Get (Optional_< Reference< Value_ >, Others_... > const& i_optional)
	        -> ::std::enable_if_t<
	            IsOptionalRefType< Optional_, Value_, Others_... >::kValue,
	            Value_& >
	{
		return ReferencePolicy::Get (*i_optional);
	}


	/**
	   @brief Checks if optional object is holding any value

	   @tparam Value_ The type of the value stored in the optional object

	   @tparam Others_ Parameters pack for handling template parameters of smart
	   pointer type that are out of scope of this optional policy.

	   @tparam Optional_ The type of the optional object

	   @param [in] i_optional Optional object to check.

	   @returns "true" if the optional object is holding a value, "false"
	   otherwise.
	*/
	template <
	    class Value_,
	    class... Others_,
	    template < class V_, class... Os_ > class Optional_ >
	static bool HasValue (Optional_< Value_, Others_... > const& i_optional)
	{
		static_assert (
		    (IsOptionalValueType< Optional_, Value_, Others_... >::kValue ||
		     IsOptionalRefType< Optional_, Value_, Others_... >::kValue),
		    "Argument type is incompatible with optional policy.");

		return bool(i_optional);
	}


	/**
	   @brief Creates an optional object holding the given value

	   @tparam Value_ The type of the value for storing in the optional object.

	   @param [in] i_value Value to store in the created optional object.

	   @returns An optional object holding the given value.
	*/
	template < class Value_ >
	static auto MakeOptional (Value_&& i_value)
	{
		return MakeOptionalImpl< ValuePointerPolicy, ClearType< Value_ > > (
		    ::std::forward< Value_ > (i_value));
	}


	/**
	   @brief Creates an optional object from the provided arguments

	   @tparam Value_ The type of the value for storing in the optional object.

	   @trapam Args_ Parameter pack of types of arguments for constructing the
	   value.

	   @param [in] i_args Arguments constructing the value.

	   @returns An optional object holding the given value.
	*/
	template < class Value_, class... Args_ >
	static auto MakeOptional (Args_&&... i_args)
	{
		return MakeOptionalImpl< ValuePointerPolicy, Value_ > (
		    ::std::forward< Args_ > (i_args)...);
	}


	/**
	   @brief Creates an optional object holding the given value

	   @tparam Value_ The type of the value a reference to which will be stored
	   the in optional object.

	   @param [in] i_value Value to store reference to it in the created
	   optional object.

	   @returns An optional object holding a reference to the given value.
	*/
	template < class Value_ >
	static auto MakeOptionalRef (Value_&& i_value)
	{
		return MakeOptionalImpl<
		    RefPointerPolicy,
		    Reference< ClearType< Value_ > > > (
		    ReferencePolicy::MakeRef (::std::forward< Value_ > (i_value)));
	}


	/**
	   @brief Creates an optional object holding the given value

	   @tparam Value_ The type of the value a const reference to which will be
	   stored in the optional object.

	   @param [in] i_value Value to store const reference to it in the created
	   optional object.

	   @returns An optional object holding a const reference to the given value.
	*/
	template < class Value_ >
	static auto MakeOptionalConstRef (Value_&& i_value)
	{
		return MakeOptionalImpl<
		    ConstRefPointerPolicy,
		    Reference< ClearType< Value_ > const > > (
		    ReferencePolicy::MakeConstRef (::std::forward< Value_ > (i_value)));
	}


private:
	template < class T_ >
	using ClearType = ::std::remove_cv_t< ::std::remove_reference_t< T_ > >;


	template <
	    template < class V_, class... Os_ > class Optional_,
	    class Value_,
	    class... Others_ >
	struct IsOptionalValueType {
		constexpr static bool kValue = ::std::is_same<
		    OptionalType< ClearType< Value_ > >,
		    Optional_< ClearType< Value_ >, Others_... > >::value;
	};


	template <
	    template < class V_, class... Os_ > class Optional_,
	    class Value_,
	    class... Others_ >
	struct IsOptionalRefType {
		constexpr static bool kValue =
		    (::std::is_same<
		         OptionalRefType< Value_ >,
		         Optional_< Reference< Value_ >, Others_... > >::value ||
		     ::std::is_same<
		         OptionalConstRefType< Value_ >,
		         Optional_< Reference< Value_ >, Others_... > >::value);
	};


	template < class SmartPointerPolicy_, class Value_, class... Args_ >
	static auto MakeOptionalImpl (Args_&&... i_args)
	{
		return SmartPointerPolicy_::template MakeSmartPointer< Value_ > (
		    ::std::forward< Args_ > (i_args)...);
	}


	template < class Value_ >
	struct OptionalTypeForValue {
		using ValueType = ::std::remove_reference_t< Value_ >;

		using Optional = ::std::conditional_t<
		    ::std::is_reference< Value_ >::value,
		    ::std::conditional_t<
		        ::std::is_const< ValueType >::value,
		        OptionalConstRefType< ValueType >,
		        OptionalRefType< ValueType > >,
		    OptionalType< Value_ > >;
	};
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif // UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_OPTIONAL_POLICY_SMARTPOINTERASOPTIONALPOLICY_HPP
