// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/factory_function_policy/StdFactoryFunctionPolicy.hpp
//
// Description: StdFactoryFunctionPolicy class declaration and definition.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_FACTORY_FUNCTION_POLICY_STDFACTORYFUNCTIONPOLICY_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_FACTORY_FUNCTION_POLICY_STDFACTORYFUNCTIONPOLICY_HPP


#include <type_traits>
#include <utility>


#include "utk/factory/v2/namespace.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


namespace detail {
	template < class >
	struct BasicFactoryFunctionFactory;


	template < class ReturnType_, class... Args_ >
	struct BasicFactoryFunctionFactory< ::std::function< ReturnType_ (Args_...) > > {
		using Function = ::std::function< ReturnType_ (Args_...) >;
		using ReturnType = ReturnType_;


		template < class Body_ >
		static Function CreateFactoryFunction (Body_&& i_body)
		{
			using BodyReturnType =
			    decltype (i_body (::std::declval< Args_ > ()...));

			static_assert (
			    (::std::is_constructible< ReturnType, BodyReturnType >::value ||
			     ::std::is_convertible< BodyReturnType, ReturnType >::value),
			    "Return type of the provided body function is incompatible "
			    "with FactoryFunction result type.");

			return [body = ::std::forward< Body_ > (i_body)](
			           Args_... i_args) -> ReturnType {
				return body (::std::forward< Args_ > (i_args)...);
			};
		}
	};


	/**
	   @brief A base for factory function type policies for using std::function
	   as factory function type

	   @tparam FactoryFunction_ The type of the factory function,
	   i.e. specialisation of std::function.

	   @tparam Production_ The actual type of the production, created by the
	   FactoryFunction_ instances. May differ from the
	   FactoryFunction_::result_type if FactoryFunction_ returns an optional
	   value.

	   @tparam kReturnsOptional_ The flag saying if the FactoryFunction_ returns
	   an optional value.

	   @note Users are not forced to use access functions provided by the policy
	   to interact with the factory functions in non-generic code.
	*/
	template <
	    class FactoryFunction_,
	    class Production_,
	    bool kReturnsOptional_ >
	struct StdFactoryFunctionPolicyBase {
		/**
		   @brief The type of the factory function, i.e. specialisation of
		   std::function
		*/
		using FactoryFunction = FactoryFunction_;

		/**
		   @internal
		   @brief The type of the factory function factory for creating
		   FactoryGunction instances
		*/
		using FactoryFunctionFactory =
		    BasicFactoryFunctionFactory< FactoryFunction >;

		/**
		   @brief The actual type of the production, created by the
		   FactoryFunction_ instances.

		   @details May differ from the FactoryFunction_::result_type if
		   FactoryFunction_ returns an optional value. If the Production_
		   template parameter value is void, then the Production is the same as
		   FactoruFunction::result_type, otherwise Production is the same as
		   Production_.
		*/
		using Production = ::std::conditional_t<
		    ::std::is_same< Production_, void >::value,
		    std::remove_reference_t< typename FactoryFunction::result_type >,
		    std::remove_reference_t< Production_ > >;

		/**
		   @brief The type of the values returnd from by FactoryFunction
		   invocation
		*/
		using ReturnType = typename FactoryFunction::result_type;

		/**
		   @brief The flag saying if the FactoryFunction_ returns an optional
		   value.
		*/
		static bool constexpr kReturnsOptional = kReturnsOptional_;


		/**
		   @brief Creates a factory function instance by wrapping the provided
		   functional object

		   @tparam Body_ The type of the functional object to wrap. Should be
		   Copyable (or Movable if rvalue reference is provided) and provide
		   operator(), taking the same arguments and returning a value of the
		   same type, as FactoryFunction.

		   @param [in] i_body The functional object to wrap into FactoryFunction
		   instance

		   @returns A factory function wrapping the privided i_body function.
		*/
		template < class Body_ >
		static FactoryFunction CreateFactoryFunction (Body_&& i_body)
		{
			return FactoryFunctionFactory::CreateFactoryFunction (
			    ::std::forward< Body_ > (i_body));
		}


		/**
		   @brief Invokes the given functional object with the provided
		   arguments

		   @tparam Function_ The type of the functional object to invoke.

		   @trapam Args_ Parameter pack of types of arguments for invoking the
		   functional object.

		   @param [in] i_function The functional object invoke.

		   @param [in] i_args Arguments for invoking the functional object.

		   @returns The result of invocation of the i_function function with the
		   provided arguments.
		*/
		template < class Function_, class... Args_ >
		static auto Invoke (Function_&& i_function, Args_&&... i_args)
		{
			return ::std::forward< Function_ > (i_function) (
			    ::std::forward< Args_ > (i_args)...);
		}
	};
} // namespace detail


/**
   @brief Base for partial specialisation
*/
template <
    class FactoryFunction_,
    class Production_ = void,
    class OptionalPolicy_ = void >
struct StdFactoryFunctionPolicy;


/**
   @brief A factory function type policies for using std::function returning
   production type instances as factory function type
*/
template < class FactoryFunction_ >
struct StdFactoryFunctionPolicy< FactoryFunction_, void, void >
    : public detail::
          StdFactoryFunctionPolicyBase< FactoryFunction_, void, false > {
	using BaseType =
	    detail::StdFactoryFunctionPolicyBase< FactoryFunction_, void, false >;

	using FactoryFunction = typename BaseType::FactoryFunction;
	using Production = typename BaseType::Production;
	using ReturnType = typename BaseType::ReturnType;


	/**
	   @brief Creates a factory function constructing production from the
	   provided arguments

	   @tparam Production_ The type of the object to construct. By default the
	   same as Production. The type should be convertible to Production or
	   Production shouldbe constructible from an instance of the type

	   @returns A factory function returning an instance of Production_
	   constructed from the arguments provided on its invocation.
	*/
	template < class Production_ = Production >
	static FactoryFunction CreateDefaultFactoryFunction ()
	{
		return BaseType::FactoryFunctionFactory::CreateFactoryFunction (
		    [](auto&&... i_args) -> ReturnType {
			    return Production_{
			        ::std::forward< decltype (i_args) > (i_args)...};
		    });
	}
};


/**
   @brief A factory function type policies for using std::function returning
   optional production type instances as factory function type
*/
template < class FactoryFunction_, class Production_, class OptionalPolicy_ >
struct StdFactoryFunctionPolicy
    : public detail::
          StdFactoryFunctionPolicyBase< FactoryFunction_, Production_, true > {
	using BaseType = detail::
	    StdFactoryFunctionPolicyBase< FactoryFunction_, Production_, true >;

	using FactoryFunction = typename BaseType::FactoryFunction;
	using Production = typename BaseType::Production;
	using ReturnType = typename BaseType::ReturnType;

	/**
	   @brief The policy that defines the optional type and basic operations
	   on its values
	*/
	using OptionalPolicy = OptionalPolicy_;

	/**
	   @brief Type for returning optional production type instances
	*/
	using OptionalType =
	    typename OptionalPolicy::template OptionalType< Production >;


	static_assert (
	    (::std::is_same< OptionalType, ReturnType >::value ||
	     ::std::is_constructible< OptionalType, ReturnType >::value ||
	     ::std::is_convertible< ReturnType, OptionalType >::value),
	    "Factory function returns optional object of type that is incompatible "
	    "with the OptionalPolicy.");


	/**
	   @brief Creates a factory function constructing optional production from
	   the provided arguments

	   @tparam Production_ The type of the object to construct. By default the
	   same as Production. The type should be convertible to Production or
	   Production shouldbe constructible from an instance of the type

	   @returns A factory function returning an optional instance of Production_
	   constructed from the arguments provided on its invocation.
	*/
	template < class Production__ = Production >
	static FactoryFunction CreateDefaultFactoryFunction ()
	{
		return BaseType::FactoryFunctionFactory::CreateFactoryFunction (
		    [](auto&&... i_args) {
			    return OptionalPolicy::template MakeOptional< Production__ > (
			        ::std::forward< decltype (i_args) > (i_args)...);
		    });
	}
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif // UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_FACTORY_FUNCTION_POLICY_STDFACTORYFUNCTIONPOLICY_HPP
