// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/ObjectRegistry_Impl.hpp
//
// Description: ObjectRegistry class implementation.


#ifndef ASCLEPIA_INCLUDE_UTK_FACTORY_V2_OBJECTREGISTRY_IMPL_HPP
#define ASCLEPIA_INCLUDE_UTK_FACTORY_V2_OBJECTREGISTRY_IMPL_HPP


#include "utk/factory/v2/ObjectRegistry_Decl.hpp"


#include <utility>


UTK_FACTORY_V2_NAMESPACE_OPEN


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
class ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    Impl {
public:
	template < class ObjectRegistry_, class Key_ >
	static auto
	    GetObject (ObjectRegistry_&& i_object_registry, Key_&& i_object_id)
	{
		auto& container =
		    ::std::forward< ObjectRegistry_ > (i_object_registry).container_;

		auto const object_it = ContainerPolicy::Find (
		    container, ::std::forward< Key_ > (i_object_id));

		if (ContainerPolicy::IsIteratorValid (container, object_it))
		{
			return OptionalPolicy::MakeOptional (
			    ContainerPolicy::IteratorValue (container, object_it));
		}

		return OptionalPolicy::template NullOptional< Object > ();
	}


	template < class ObjectRegistry_, class Key_ >
	static auto
	    GetObjectRef (ObjectRegistry_&& i_object_registry, Key_&& i_object_id)
	{
		auto& container =
		    ::std::forward< ObjectRegistry_ > (i_object_registry).container_;

		auto const object_it = ContainerPolicy::Find (container, i_object_id);

		if (ContainerPolicy::IsIteratorValid (container, object_it))
		{
			return OptionalPolicy::MakeOptionalRef (
			    ContainerPolicy::IteratorValue (container, object_it));
		}

		return OptionalPolicy::template NullOptional< Object& > ();
	}


	template < class ObjectRegistry_, class Key_ >
	static auto GetObjectConstRef (
	    ObjectRegistry_&& i_object_registry, Key_&& i_object_id)
	{
		auto& container =
		    ::std::forward< ObjectRegistry_ > (i_object_registry).container_;

		auto const object_it = ContainerPolicy::Find (container, i_object_id);

		if (ContainerPolicy::IsIteratorValid (container, object_it))
		{
			return OptionalPolicy::MakeOptionalConstRef (
			    ContainerPolicy::IteratorValue (container, object_it));
		}

		return OptionalPolicy::template NullOptional< Object const& > ();
	}
};


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    ObjectRegistry ()
{
	ContainerPolicy::InitContainer (container_);
}


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    ~ObjectRegistry () = default;


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
template < class Key_, class Value_ >
bool ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    AddObject (Key_&& i_object_id, Value_&& i_object)
{
	/**
	   @todo FIXME: Impossible to add object if the value passed as i_object_id
	   is a const named variable. Need to provide separate specialisations for
	   rvalue-reverences and const references.
	*/
	return ContainerPolicy::Emplace (
	    container_,
	    ::std::forward< ObjectId_ > (i_object_id),
	    ::std::forward< Object_ > (i_object));
};


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
template < class Key_, class Value_ >
void ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    AddOrReplaceObject (Key_&& i_object_id, Value_&& i_object)
{
	ContainerPolicy::AddOrReplace (
	    container_,
	    ::std::forward< ObjectId_ > (i_object_id),
	    ::std::forward< Object_ > (i_object));
};


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
template < class Key_ >
void ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    RemoveObject (Key_&& i_object_id)
{
	ContainerPolicy::Erase (
	    container_, ::std::forward< ObjectId_ > (i_object_id));
};


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
template < class Key_, class ObjectType_ >
auto ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    GetObject (Key_&& i_object_id) -> ::std::enable_if_t<
        ::std::is_copy_constructible< ObjectType_ >::value,
        OptionalObject >
{
	return Impl::GetObject (*this, ::std::forward< Key_ > (i_object_id));
}


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
template < class Key_, class ObjectType_ >
auto ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    GetObject (Key_&& i_object_id) const -> ::std::enable_if_t<
        ::std::is_copy_constructible< ObjectType_ >::value,
        OptionalObject >
{
	return Impl::GetObject (*this, ::std::forward< Key_ > (i_object_id));
}


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
template < class Key_ >
auto ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    GetObjectRef (Key_&& i_object_id) -> OptionalObjectRef
{
	return Impl::GetObjectRef (*this, ::std::forward< Key_ > (i_object_id));
}


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
template < class Key_ >
auto ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    GetObjectRef (Key_&& i_object_id) const -> OptionalObjectRef
{
	return Impl::GetObjectRef (*this, ::std::forward< Key_ > (i_object_id));
}


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
template < class Key_ >
auto ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    GetObjectConstRef (Key_&& i_object_id) -> OptionalObjectConstRef
{
	return Impl::GetObjectConstRef (
	    *this, ::std::forward< Key_ > (i_object_id));
}


template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
template < class Key_ >
auto ObjectRegistry< ObjectId_, Object_, OptionalPolicy_, ContainerPolicy_ >::
    GetObjectConstRef (Key_&& i_object_id) const -> OptionalObjectConstRef
{
	return Impl::GetObjectConstRef (
	    *this, ::std::forward< Key_ > (i_object_id));
}


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif /* ASCLEPIA_INCLUDE_UTK_FACTORY_V2_OBJECTREGISTRY_IMPL_HPP */
