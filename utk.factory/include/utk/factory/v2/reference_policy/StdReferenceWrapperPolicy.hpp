// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/reference_policy/StdReferenceWrapperPolicy.hpp
//
// Description: StdReferenceWrapperPolicy class definition.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_REFERENCE_POLICY_STDREFERENCEWRAPPERPOLICY_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_REFERENCE_POLICY_STDREFERENCEWRAPPERPOLICY_HPP


#include <functional>
#include <utility>

#include "utk/factory/v2/namespace.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief A reference type policy for using std::reference_wrapper for storing
   references

   @note Users are not forced to use access functions provided by the policy to
   interact with the optional values in non-generic code.
*/
struct StdReferenceWrapperPolicy {
	/**
	   @brief A reference type provided by the policy

	   @tparam T_ The type of the object referenced by the stored reference.
	*/
	template < class T_ >
	using Reference = ::std::reference_wrapper< T_ >;


	/**
	   @brief Gets the reference stored in the given reference object

	   @tparam Value_ The type of the object referenced by the stored reference.

	   @returns A reference stored in the given reference object.
	*/
	template < class Value_ >
	static auto& Get (Reference< Value_ > const& i_reference)
	{
		return i_reference.get ();
	}


	/**
	   @brief Constructs an object hoding a reference to the given value

	   @tparam Value_ The type of the object to store reference to.

	   @returns A reference object holding a reference to the given object.
	*/
	template < class Value_ >
	static auto MakeRef (Value_&& i_value)
	{
		return ::std::ref (::std::forward< Value_ > (i_value));
	}


	/**
	   @brief Constructs an object hoding a const reference to the given value

	   @tparam Value_ The type of the object to store reference to.

	   @returns A reference object holding a const reference to the given
	   object.
	*/
	template < class Value_ >
	static auto MakeConstRef (Value_&& i_value)
	{
		return ::std::cref (::std::forward< Value_ > (i_value));
	}
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif // UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_REFERENCE_POLICY_STDREFERENCEWRAPPERPOLICY_HPP
