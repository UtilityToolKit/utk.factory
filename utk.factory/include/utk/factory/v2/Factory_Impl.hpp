#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_FACTORY_IMPL_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_FACTORY_IMPL_HPP


#include "utk/factory/v2/Factory_Decl.hpp"
#include "utk/factory/v2/ObjectRegistry_Impl.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


namespace detail {
	struct BypassOptional {
		template < class Optional_ >
		static auto MakeOptional (Optional_&& i_optional)
		{
			return ::std::forward< Optional_ > (i_optional);
		}
	};
} // namespace detail

template <
    class ProductionId_,
    class FactoryFunctionPolicy_,
    class LocalRegistryPolicy_,
    class GlobalRegistryPolicy_,
    class OptionalPolicy_ >
template < bool kUseGlobalRegistry_ >
class Factory<
    ProductionId_,
    FactoryFunctionPolicy_,
    LocalRegistryPolicy_,
    GlobalRegistryPolicy_,
    OptionalPolicy_ >::
    GlobalContext<
        kUseGlobalRegistry_,
        ::std::enable_if_t< kUseGlobalRegistry_ > > {
public:
	template < class... AddObjectArgs_ >
	static bool RegisterProduction (AddObjectArgs_&&... i_args)
	{
		if (GlobalRegistryProvider::HaveGlobalRegistry ())
		{
			return GlobalRegistryProvider::GlobalRegistry ().AddObject (
			    ::std::forward< AddObjectArgs_ > (i_args)...);
		}

		return false;
	}

	template < class... AddOrReplaceObjectArgs_ >
	static void
	    RegisterOrReplaceProduction (AddOrReplaceObjectArgs_&&... i_args)
	{
		if (GlobalRegistryProvider::HaveGlobalRegistry ())
		{
			GlobalRegistryProvider::GlobalRegistry ().AddOrReplaceObject (
			    ::std::forward< AddOrReplaceObjectArgs_ > (i_args)...);
		}
	}

	template < class... RemoveObjectArgs_ >
	static void UnregisterProduction (RemoveObjectArgs_&&... i_args)
	{
		if (GlobalRegistryProvider::HaveGlobalRegistry ())
		{
			GlobalRegistryProvider::GlobalRegistry ().RemoveObject (
			    ::std::forward< RemoveObjectArgs_ > (i_args)...);
		}
	}


private:
	friend class Factory;

	template < bool kEnable_, class Id_, class... Args_ >
	static auto Create (Id_&& i_production_id, Args_&&... i_args)
	    -> ::std::enable_if_t< kEnable_, OptionalProduction >
	{
		if (GlobalRegistryProvider::HaveGlobalRegistry ())
		{
			using RegistryOptionalPolicy =
			    typename GlobalFactoryRegistry::OptionalPolicy;

			using ReturnOptionalPolicy = std::conditional_t<
			    FactoryFunctionPolicy::kReturnsOptional,
			    detail::BypassOptional,
			    OptionalPolicy >;

			auto maybe_factory_function =
			    GlobalRegistryProvider::GlobalRegistry ().GetObjectRef (
			        ::std::forward< Id_ > (i_production_id));

			if (RegistryOptionalPolicy::HasValue (maybe_factory_function))
			{
				return ReturnOptionalPolicy::MakeOptional (
				    FactoryFunctionPolicy::Invoke (
				        RegistryOptionalPolicy::Get (maybe_factory_function),
				        std::forward< Args_ > (i_args)...));
			}
		}

		return OptionalPolicy::template NullOptional< Production > ();
	}

	template < bool kEnable_, class Id_, class... Args_ >
	static auto Create (Id_&& i_production_id, Args_&&... i_args)
	    -> ::std::enable_if_t< !kEnable_, OptionalProduction >
	{
		return OptionalPolicy::template NullOptional< Production > ();
	}
};


template <
    class ProductionId_,
    class FactoryFunctionPolicy_,
    class LocalRegistryPolicy_,
    class GlobalRegistryPolicy_,
    class OptionalPolicy_ >
Factory<
    ProductionId_,
    FactoryFunctionPolicy_,
    LocalRegistryPolicy_,
    GlobalRegistryPolicy_,
    OptionalPolicy_ >::Factory () = default;


template <
    class ProductionId_,
    class FactoryFunctionPolicy_,
    class LocalRegistryPolicy_,
    class GlobalRegistryPolicy_,
    class OptionalPolicy_ >
Factory<
    ProductionId_,
    FactoryFunctionPolicy_,
    LocalRegistryPolicy_,
    GlobalRegistryPolicy_,
    OptionalPolicy_ >::~Factory () = default;


template <
    class ProductionId_,
    class FactoryFunctionPolicy_,
    class LocalRegistryPolicy_,
    class GlobalRegistryPolicy_,
    class OptionalPolicy_ >
template < bool kUseGlobal_, bool kUseLocal_, class Id_, class... Args_ >
auto Factory<
    ProductionId_,
    FactoryFunctionPolicy_,
    LocalRegistryPolicy_,
    GlobalRegistryPolicy_,
    OptionalPolicy_ >::CreateImpl (Id_&& i_production_id, Args_&&... i_args)
    -> ::std::enable_if_t< kUseGlobal_ && kUseLocal_, OptionalProduction >
{
	{
		auto maybe_global_produced =
		    Global::template Create< kPreferGlobalRegistry > (
		        i_production_id, i_args...);

		if (OptionalPolicy::HasValue (maybe_global_produced))
		{
			return maybe_global_produced;
		}
	}

	{
		using RegistryOptionalPolicy =
		    typename LocalFactoryRegistry::OptionalPolicy;

		using ReturnOptionalPolicy = std::conditional_t<
		    FactoryFunctionPolicy::kReturnsOptional,
		    detail::BypassOptional,
		    OptionalPolicy >;

		auto maybe_factory_function = registry_.GetObjectRef (i_production_id);

		if (RegistryOptionalPolicy::HasValue (maybe_factory_function))
		{
			return ReturnOptionalPolicy::MakeOptional (
			    FactoryFunctionPolicy::Invoke (
			        RegistryOptionalPolicy::Get (maybe_factory_function),
			        std::forward< Args_ > (i_args)...));
		}
	}

	return Global::template Create< !kPreferGlobalRegistry > (
	    ::std::forward< Id_ > (i_production_id),
	    ::std::forward< Args_ > (i_args)...);
}


template <
    class ProductionId_,
    class FactoryFunctionPolicy_,
    class LocalRegistryPolicy_,
    class GlobalRegistryPolicy_,
    class OptionalPolicy_ >
template < bool kUseGlobal_, bool kUseLocal_, class Id_, class... Args_ >
auto Factory<
    ProductionId_,
    FactoryFunctionPolicy_,
    LocalRegistryPolicy_,
    GlobalRegistryPolicy_,
    OptionalPolicy_ >::CreateImpl (Id_&& i_production_id, Args_&&... i_args)
    -> ::std::enable_if_t< !kUseGlobal_ && kUseLocal_, OptionalProduction >
{
	using RegistryOptionalPolicy =
	    typename LocalFactoryRegistry::OptionalPolicy;

	auto maybe_factory_function =
	    registry_.GetObjectRef (::std::forward< Id_ > (i_production_id));

	if (RegistryOptionalPolicy::HasValue (maybe_factory_function))
	{
		using ReturnOptionalPolicy = std::conditional_t<
		    FactoryFunctionPolicy::kReturnsOptional,
		    detail::BypassOptional,
		    OptionalPolicy >;

		return ReturnOptionalPolicy::MakeOptional (
		    FactoryFunctionPolicy::Invoke (
		        RegistryOptionalPolicy::Get (maybe_factory_function),
		        std::forward< Args_ > (i_args)...));
	}

	return OptionalPolicy::template NullOptional< Production > ();
}


template <
    class ProductionId_,
    class FactoryFunctionPolicy_,
    class LocalRegistryPolicy_,
    class GlobalRegistryPolicy_,
    class OptionalPolicy_ >
template < bool kUseGlobal_, bool kUseLocal_, class Id_, class... Args_ >
auto Factory<
    ProductionId_,
    FactoryFunctionPolicy_,
    LocalRegistryPolicy_,
    GlobalRegistryPolicy_,
    OptionalPolicy_ >::CreateImpl (Id_&& i_production_id, Args_&&... i_args)
    -> ::std::enable_if_t< kUseGlobal_ && !kUseLocal_, OptionalProduction >
{
	return Global::template Create< kUseGlobal_ > (
	    ::std::forward< Id_ > (i_production_id),
	    ::std::forward< Args_ > (i_args)...);
}


template <
    class ProductionId_,
    class FactoryFunctionPolicy_,
    class LocalRegistryPolicy_,
    class GlobalRegistryPolicy_,
    class OptionalPolicy_ >
template < bool kUseLocal_, class Id_, class Function_ >
auto Factory<
    ProductionId_,
    FactoryFunctionPolicy_,
    LocalRegistryPolicy_,
    GlobalRegistryPolicy_,
    OptionalPolicy_ >::
    RegisterProductionImpl (
        Id_&& i_production_id, Function_&& i_factory_function)
        -> ::std::enable_if_t< kUseLocal_, bool >
{
	return registry_.AddObject (
	    ::std::forward< Id_ > (i_production_id),
	    ::std::forward< Function_ > (i_factory_function));
};


template <
    class ProductionId_,
    class FactoryFunctionPolicy_,
    class LocalRegistryPolicy_,
    class GlobalRegistryPolicy_,
    class OptionalPolicy_ >
template < bool kUseLocal_, class Id_, class Function_ >
auto Factory<
    ProductionId_,
    FactoryFunctionPolicy_,
    LocalRegistryPolicy_,
    GlobalRegistryPolicy_,
    OptionalPolicy_ >::
    RegisterOrReplaceProductionImpl (
        Id_&& i_production_id, Function_&& i_factory_function)
        -> ::std::enable_if_t< kUseLocal_, void >
{
	registry_.AddOrReplaceObject (
	    ::std::forward< Id_ > (i_production_id),
	    ::std::forward< Function_ > (i_factory_function));
};


template <
    class ProductionId_,
    class FactoryFunctionPolicy_,
    class LocalRegistryPolicy_,
    class GlobalRegistryPolicy_,
    class OptionalPolicy_ >
template < bool kUseLocal_, class Id_ >
auto Factory<
    ProductionId_,
    FactoryFunctionPolicy_,
    LocalRegistryPolicy_,
    GlobalRegistryPolicy_,
    OptionalPolicy_ >::UnregisterProductionImpl (Id_&& i_production_id)
    -> ::std::enable_if_t< kUseLocal_, void >
{
	registry_.RemoveObject (::std::forward< Id_ > (i_production_id));
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif /* UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_FACTORY_IMPL_HPP */
