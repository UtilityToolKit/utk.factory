// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/ObjectRegistry_Decl.hpp
//
// Description: ObjectRegistry class declaration.


#ifndef ASCLEPIA_INCLUDE_UTK_FACTORY_V2_OBJECTREGISTRY_DECL_HPP
#define ASCLEPIA_INCLUDE_UTK_FACTORY_V2_OBJECTREGISTRY_DECL_HPP


#include <type_traits>

#include "utk/factory/v2/namespace.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief An object registry organaised as key-value storage

   @tparam ObjectId_ The type of the keys used to identify objects.

   @tparam Object_ The type of the values stored in registry.

   @tparam OptionalPolicy_ The policy that defines the optional type and basic
   operations on its values. Optional type values are used to return requested
   objects.

   @tparam ContainerPolicy_ The policy that defines the container type for
   storing key-value pairs and operations on container type instances.
*/
template <
    class ObjectId_,
    class Object_,
    class OptionalPolicy_,
    class ContainerPolicy_ >
class ObjectRegistry {
public:
	/**
	   @brief Type of the keys used to identify objects
	*/
	using ObjectId = ObjectId_;

	/**
	   @brief Type of the values stored in registry
	*/
	using Object = Object_;

	/**
	   @brief The policy that defines the optional type and basic operations on
	   its values
	*/
	using OptionalPolicy = OptionalPolicy_;

	/**
	   @brief Type for returning copies of the objects stored in registry
	*/
	using OptionalObject =
	    typename OptionalPolicy::template OptionalType< Object >;

	/**
	   @brief Type for returning references to the objects stored in registry
	*/
	using OptionalObjectRef =
	    typename OptionalPolicy::template OptionalRefType< Object >;

	/**
	   @brief Type for returning const references to the objects stored in
	   registry
	*/
	using OptionalObjectConstRef =
	    typename OptionalPolicy::template OptionalConstRefType< Object >;

	using ContainerPolicy = ContainerPolicy_;

	/**
	   @brief Type of the container for storing values
	*/
	using Container =
	    typename ContainerPolicy::template Container< ObjectId, Object >;


	ObjectRegistry ();

	~ObjectRegistry ();

	/**
	   @brief Adds an object with the given id to the registry if it does not
	   contain another object with the same id

	   @tparam Key_ Type of the id of the object to add to the registry

	   @tparam Value_ Type of the object to add to the registry

	   @param [in] i_object_id The id of the object to add to the registry

	   @param [in] i_object The object to add to the registry

	   @returns "true" if the object was successfully added to the registry,
	   "false" otherwise
	*/
	template < class Key_, class Value_ >
	bool AddObject (Key_&& i_object_id, Value_&& i_object);

	/**
	   @brief Adds an object with the given id to the registry if it does not
	   contain another object with the same id or replaces the existing object

	   @tparam Key_ Type of the id of the object to add to the registry

	   @tparam Value_ Type of the object to add to the registry

	   @param [in] i_object_id The id of the object to add to the registry

	   @param [in] i_object The object to add to the registry
	*/
	template < class Key_, class Value_ >
	void AddOrReplaceObject (Key_&& i_object_id, Value_&& i_object);

	/**
	   @brief Removes an object with the given id from the registry

	   @tparam Key_ Type of the id of the object to remove from the registry

	   @param [in] i_object_id The id of the object to remove from the registry
	*/
	template < class Key_ >
	void RemoveObject (Key_&& i_object_id);

	/**
	   @brief Requests a copy of the object with the given id

	   @tparam Key_ Type of the id of the object to request from the registry

	   @param [in] i_object_id The id of the object to request from the registry

	   @returns An optional object with the copy of the object stored in the
	   registry with the given id if any, empty optional object otherwise.

	   @note This function is only available if the objects in the registry
	   arecopy-constructible
	*/
	template < class Key_, class ObjectType_ = Object >
	::std::enable_if_t<
	    ::std::is_copy_constructible< ObjectType_ >::value,
	    OptionalObject >
	    GetObject (Key_&& i_object_id);

	template < class Key_, class ObjectType_ = Object >
	::std::enable_if_t<
	    ::std::is_copy_constructible< ObjectType_ >::value,
	    OptionalObject >
	    GetObject (Key_&& i_object_id) const;

	/**
	   @brief Requests a reference to the object with the given id

	   @tparam Key_ Type of the id of the object to request from the registry

	   @param [in] i_object_id The id of the object to request from the registry

	   @returns An optional object with the reference to the object stored in
	   the registry with the given id if any, empty optional object otherwise.
	*/
	template < class Key_ >
	OptionalObjectRef GetObjectRef (Key_&& i_object_id);

	template < class Key_ >
	OptionalObjectRef GetObjectRef (Key_&& i_object_id) const;

	/**
	   @brief Requests a const reference to the object with the given id

	   @tparam Key_ Type of the id of the object to request from the registry

	   @param [in] i_object_id The id of the object to request from the registry

	   @returns An optional object with the const reference to the object stored
	   in the registry with the given id if any, empty optional object
	   otherwise.
	*/
	template < class Key_ >
	OptionalObjectConstRef GetObjectConstRef (Key_&& i_object_id);

	template < class Key_ >
	OptionalObjectConstRef GetObjectConstRef (Key_&& i_object_id) const;


private:
	class Impl;


	Container container_;
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif /* ASCLEPIA_INCLUDE_UTK_FACTORY_V2_OBJECTREGISTRY_DECL_HPP */
