// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/smart_pointer_policy/StdSmartPointerPolicy.hpp
//
// Description: StdSmartPointerPolicy class definition.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_SMART_POINTER_POLICY_STDSMARTPOINTERPOLICY_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_SMART_POINTER_POLICY_STDSMARTPOINTERPOLICY_HPP


#include <memory>
#include <type_traits>

#include "utk/factory/v2/namespace.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief A smart pointer type policy for using std::shared_ptr and
   std::unique_ptr as smart pointers type

   @tparam SmartPointer_ The smart pointer type to use. May be std::shared_ptr
   or partial specialisation of std::unique_ptr. In case of std::unique_ptr a
   partial specialisation is requred to account for template parameters other
   then element_type.

   @note Users are not forced to use access functions provided by the policy to
   interact with the optional values in non-generic code.
*/
template < template < class T_ > class SmartPointer_ >
class StdSmartPointerPolicy {
private:
	template < class Value_ >
	struct IsStdSharedPtr;

	template < class Value_ >
	struct IsStdUniquePtr;


public:
	/**
	   @brief A smart pointer type provided by the policy

	   @tparam T_ The type of the object controlled by the smart pointer object
	   (its element_type).
	*/
	template < class T_ >
	using SmartPointer = SmartPointer_< T_ >;


	/**
	   @brief Constructs a smart pointer from the provided arguments

	   @details This specialisation is used when SmartPointer is std::unique_ptr
	   with non-default deleter.

	   @tparam Value_ The type of the object controlled by the constructed smart
	   pointer object (its element_type).

	   @tparam Args_ Parameter pack of arguments for constructing the controller
	   object.

	   @returns A smart pointer object controlling an object of type Value_
	   constructed from the provided arguments.
	*/
	template < class Value_, class... Args_ >
	static auto MakeSmartPointer (Args_&&... i_args) -> ::std::enable_if_t<
	    (!IsStdSharedPtr< Value_ >::kValue &&
	     !IsStdUniquePtr< Value_ >::kValue),
	    SmartPointer< Value_ > >
	{
		return SmartPointer< Value_ > (
		    new Value_ (::std::forward< Args_ > (i_args)...));
	}


	/**
	   @brief Constructs a smart pointer from the provided arguments

	   @details This specialisation is used when SmartPointer is
	   std::shared_ptr.

	   @tparam Value_ The type of the object controlled by the constructed smart
	   pointer object (its element_type).

	   @tparam Args_ Parameter pack of arguments for constructing the controller
	   object.

	   @returns A smart pointer object controlling an object of type Value_
	   constructed from the provided arguments.
	*/
	template < class Value_, class... Args_ >
	static auto MakeSmartPointer (Args_&&... i_args) -> ::std::
	    enable_if_t< IsStdSharedPtr< Value_ >::kValue, SmartPointer< Value_ > >
	{
		return ::std::make_shared< Value_ > (
		    ::std::forward< Args_ > (i_args)...);
	}


	/**
	   @brief Constructs a smart pointer from the provided arguments

	   @details This specialisation is used when SmartPointer is std::unique_ptr
	   with default deleter.

	   @tparam Value_ The type of the object controlled by the constructed smart
	   pointer object (its element_type).

	   @tparam Args_ Parameter pack of arguments for constructing the controller
	   object.

	   @returns A smart pointer object controlling an object of type Value_
	   constructed from the provided arguments.
	*/
	template < class Value_, class... Args_ >
	static auto MakeSmartPointer (Args_&&... i_args) -> ::std::
	    enable_if_t< IsStdUniquePtr< Value_ >::kValue, SmartPointer< Value_ > >
	{
		return ::std::make_unique< Value_ > (
		    ::std::forward< Args_ > (i_args)...);
	}


private:
	template < class Value_ >
	struct IsStdSharedPtr {
		constexpr static bool kValue = ::std::is_same<
		    ::std::shared_ptr< Value_ >,
		    SmartPointer< Value_ > >::value;
	};


	template < class Value_ >
	struct IsStdUniquePtr {
		constexpr static bool kValue = ::std::is_same<
		    ::std::unique_ptr< Value_ >,
		    SmartPointer< Value_ > >::value;
	};
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif // UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_SMART_POINTER_POLICY_STDSMARTPOINTERPOLICY_HPP
