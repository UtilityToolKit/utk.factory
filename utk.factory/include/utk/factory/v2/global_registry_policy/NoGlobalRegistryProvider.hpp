// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/global_registry_policy/NoGlobalRegistryProvider.hpp
//
// Description: NoGlobalRegistryProvider class declaration.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_GLOBAL_REGISTRY_POLICY_NOGLOBALREGISTRYPROVIDER_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_GLOBAL_REGISTRY_POLICY_NOGLOBALREGISTRYPROVIDER_HPP


#include "utk/factory/v2/namespace.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief A global factory function registry provider type that provides no
   global registry
*/
template < class Registry_ >
struct NoGlobalRegistryProvider {
	/**
	   @brief The flag defining if the registry provider can potentially provide
	   global registry instance or not

	   @details Unset for this provider.
	*/
	static bool constexpr kCanPotentiallyProvide = false;


	/**
	   @brief Provides access to the global factory function registry instance

	   @details Undefined for this provider.
	*/
	static Registry_& GlobalRegistry ();


	/**
	   @brief Checks if the registry provider have a global registry
	   instance to provid or not in runtime

	   @returns Always returns "false" for this registry provider.
	*/
	static bool constexpr HaveGlobalRegistry ()
	{
		return false;
	}
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif // UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_GLOBAL_REGISTRY_POLICY_NOGLOBALREGISTRYPROVIDER_HPP
