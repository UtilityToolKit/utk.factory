// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/global_registry_policy/GlobalRegistryPolicy.hpp
//
// Description: GlobalRegistryPolicy class declaration.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_GLOBAL_REGISTRY_POLICY_GLOBALREGISTRYPOLICY_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_GLOBAL_REGISTRY_POLICY_GLOBALREGISTRYPOLICY_HPP


#include "utk/factory/v2/namespace.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief A generic policy defining how to operate on the global factory
   function registry

   @tparam Registry_ The type of the global factory function registry.

   @tparam RegistryProvider_ The type of the factory function global registry
   instance provider.

   @tparam kPreferGlobalRegistry_ The flag defining if the global registry is
   prefered over local when creating objects.
*/
template <
    template < class ObjectId_, class Object_ > class Registry_,
    template < class RegistryToProvide_ > class RegistryProvider_,
    bool kPreferGlobalRegistry_ >
struct GlobalRegistryPolicy {
	/**
	   @brief The type of the global factory function registry

	   @tparam ObjectId_ The type of the keys used to identify objects.

	   @tparam Object_ The type of the values stored in registry.
	*/
	template < class ObjectId_, class Object_ >
	using Registry = Registry_< ObjectId_, Object_ >;

	/**
	   @brief The type of the factory function global registry instance provider

	   @tparam RegistryToProvide_ The type of the registry to provide.
	*/
	template < class RegistryToProvide_ >
	using RegistryProvider = RegistryProvider_< RegistryToProvide_ >;

	/**
	   @brief The flag defining if global registry is used or not

	   @details Set for this policy.
	*/
	static bool constexpr kUseGlobalRegistry = true;

	/**
	   @brief The flag defining if the global registry is prefered over local
	   when creating objects
	*/
	static bool constexpr kPreferGlobalRegistry = kPreferGlobalRegistry_;
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif // UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_GLOBAL_REGISTRY_POLICY_GLOBALREGISTRYPOLICY_HPP
