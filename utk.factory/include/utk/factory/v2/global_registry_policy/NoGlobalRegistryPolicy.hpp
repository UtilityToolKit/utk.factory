// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/global_registry_policy/NoGlobalRegistryPolicy.hpp
//
// Description: NoGlobalRegistryPolicy class declaration.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_GLOBAL_REGISTRY_POLICY_NOGLOBALREGISTRYPOLICY_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_GLOBAL_REGISTRY_POLICY_NOGLOBALREGISTRYPOLICY_HPP


#include "utk/factory/v2/namespace.hpp"

#include "utk/factory/v2/global_registry_policy/NoGlobalRegistryProvider.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief A policy for not using global registry
*/
struct NoGlobalRegistryPolicy {
	/**
	   @brief Dummy registry type that does nothing
	*/
	template < class, class >
	struct Registry {
	};

	/**
	   @brief A global factory function registry provider type that provides no
	   global registry
	*/
	template < class Registry_ >
	using RegistryProvider = NoGlobalRegistryProvider< Registry_ >;

	/**
	   @brief The flag defining if global registry is used or not

	   @details Unset for this policy.
	*/
	static bool constexpr kUseGlobalRegistry = false;

	/**
	   @brief The flag defining if the global registry is prefered over local
	   when creating objects

	   @details Unset for this policy.
	*/
	static bool constexpr kPreferGlobalRegistry = false;
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif // UTK_FACTORY_INCLUDE_UTK_FACTORY_V2_GLOBAL_REGISTRY_POLICY_NOGLOBALREGISTRYPOLICY_HPP
