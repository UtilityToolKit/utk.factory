// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v2/Factory_Decl.hpp
//
// Description: Factory class declaration.


#ifndef ASCLEPIA_INCLUDE_UTK_FACTORY_V2_FACTORY_DECL_HPP
#define ASCLEPIA_INCLUDE_UTK_FACTORY_V2_FACTORY_DECL_HPP


#include <type_traits>
#include <utility>


#include "utk/factory/v2/namespace.hpp"

#include "utk/factory/v2/ObjectRegistry_Decl.hpp"


UTK_FACTORY_V2_NAMESPACE_OPEN


/**
   @brief An object factory backed by a local (per-instance) or global
   registry of factory functions, or both

   @tparam ProductionId_ The type of the keys used to identify different
   producible objects.

   @tparam FactoryFunctionPolicy_ The policy defining the type of factory
   functions and its properties. If factory functions produce optional objects
   then FactoryFunctionPolicy_ may provide optional policy for using the
   returned optional objects. This policy will be used as Factory
   OptionalPolicy_.

   @tparam LocalRegistryPolicy_ The policy defining the type of the local
   registry and how it's used.

   @tparam GlobalRegistryPolicy_ The policy defining the type of the global
   registry and how it's used.

   @tparam OptionalPolicy_ The policy that defines the optional type and basic
   operations on its values. Optional type values are used to return requested
   objects.
*/
template <
    class ProductionId_,
    class FactoryFunctionPolicy_,
    class LocalRegistryPolicy_,
    class GlobalRegistryPolicy_,
    class OptionalPolicy_ = typename FactoryFunctionPolicy_::OptionalPolicy >
class Factory {
private:
	template < bool kUseGlobalRegistry_, class SfinaeTag_ >
	class GlobalContext;

	template < bool kUseGlobalRegistry_, class = void >
	class GlobalContext;

	template < bool kUseGlobalRegistry_ >
	class GlobalContext<
	    kUseGlobalRegistry_,
	    ::std::enable_if_t< kUseGlobalRegistry_ > >;


public:
	/**
	   @brief The type of the keys used to identify different producible objects
	*/
	using ProductionId = ProductionId_;

	/**
	   @brief The policy defining the type of factory functions and its
	   properties

	   @details If factory functions produce optional objects then
	   FactoryFunctionPolicy_ may provide optional policy for using the returned
	   optional objects. This policy will be used as Factory OptionalPolicy_.
	*/
	using FactoryFunctionPolicy = FactoryFunctionPolicy_;

	/**
	   @brief Type of the objects, produced by the factory

	   @detail Value of this type is the payload of the optional object,
	   returned in responce for object creation request. In case where factory
	   is used to produce objects of classes inherited from common base class
	   this common base class should be used as the Production type.
	*/
	using Production = typename FactoryFunctionPolicy::Production;

	/**
	   @brief The type of the functional objects used to create production
	*/
	using FactoryFunction = typename FactoryFunctionPolicy::FactoryFunction;

	/**
	   @brief The policy that defines the optional type and basic operations on
	   its values
	*/
	using OptionalPolicy = OptionalPolicy_;

	/**
	   @brief Type for returning optional objects
	*/
	template < class Value_ >
	using OptionalType =
	    typename OptionalPolicy::template OptionalType< Value_ >;

	/**
	   @brief Type for returning the created objects
	*/
	using OptionalProduction = OptionalType< Production >;

	/**
	   @brief The policy defining the type of the local registry and how it's
	   used
	*/
	using LocalRegistryPolicy = LocalRegistryPolicy_;

	/**
	   @brief The policy defining the type of the global registry and how it's
	   used
	*/
	using GlobalRegistryPolicy = GlobalRegistryPolicy_;

	/**
	   @brief The flag defining if local (per-instance) registry is used or not

	   @details When set, Factory has methods for registering, replacing and
	   unregistering production on per-instance basis.
	*/
	static bool constexpr kUseLocalRegistry =
	    LocalRegistryPolicy::kUseLocalRegistry;

	/**
	   @brief The type of the local (per-instance) registry

	   @details If LocalRegistryPolicy demands not using local registry, it
	   should provide dummy registry type, parametrised by 2 types. An instance
	   of this type will be stored in each Factory instance.
	*/
	using LocalFactoryRegistry = typename LocalRegistryPolicy::
	    template Registry< ProductionId, FactoryFunction >;

	/**
	   @brief The flag defining if global registry is used or not

	   @details When set, Factory defines nested Global type for accessing
	   global registry, provided by the GlobalRegistryProvider. Otherwise trying
	   to use Factory::Global results in compilation error.
	*/
	static bool constexpr kUseGlobalRegistry =
	    GlobalRegistryPolicy::kUseGlobalRegistry;

	/**
	   @brief The flag defining if the global registry is prefered over local
	   when creating objects

	   @details When set, calling Factory::Create() will result in trying to
	   create requested production object with the factory function, provided by
	   the global registry, if one is used.
	*/
	static bool constexpr kPreferGlobalRegistry =
	    GlobalRegistryPolicy::kPreferGlobalRegistry;

	/**
	   @brief The type of the global registry

	   @details If GlobalRegistryPolicy demands not using global registry, it
	   should provide dummy registry type, parametrised by 2 types.
	*/
	using GlobalFactoryRegistry = typename GlobalRegistryPolicy::
	    template Registry< ProductionId, FactoryFunction >;

	/**
	   @brief The type of the global registry provider

	   @details This class is used for accessing the global factory function
	   registry:

	    * static GlobalRegistryProvider::HaveGlobalRegistry() should return
	   "true" if the provider actually holds the global registry, and "false"
	   otherwise.

	    * static GlobalRegistryProvider::GlobalRegistry() should return a
	   non-const reference to the global registry object of type
	   GlobalFactoryRegistry;

	   If GlobalRegistryPolicy demands not using global registry, it should
	   provide dummy registry type, parametrised by 2 types.
	*/
	using GlobalRegistryProvider =
	    typename GlobalRegistryPolicy::template RegistryProvider<
	        GlobalFactoryRegistry >;

	/**
	   @brief Provides access functions for the global registry

	   @details Trying to access global registry when it is not used results in
	   compilation error.

	   Provides the same functions for registering, replacing and unregistering
	   as Factory.
	*/
	using Global = GlobalContext< kUseGlobalRegistry >;


	static_assert (
	    kUseGlobalRegistry || kUseLocalRegistry,
	    "Both global and local production registries are disabled. "
	    "Object creation is impossible.");

	static_assert (
	    (kUseLocalRegistry ||
	     (kUseGlobalRegistry &&
	      GlobalRegistryProvider::kCanPotentiallyProvide)),
	    "Local production registry is disabled while global registry provider "
	    "cannot potentially provide global registry. Object creation is "
	    "impossible.");

	static_assert (
	    (!FactoryFunctionPolicy::kReturnsOptional ||
	     ::std::is_same<
	         OptionalType< Production >,
	         typename FactoryFunctionPolicy::ReturnType >::value),
	    "Factory function returns optional object of type incompatible with "
	    "the OptionalPolicy provided by the FactoryFunctionPolicy");


	Factory ();
	~Factory ();


	/**
	   @brief Requests creation of the production with the given id from the
	   provided arguments

	   @tparam Id_ The type of the id of the production to create

	   @trapam Args_ Parameter pack of types of arguments for constructing the
	   production.

	   @param [in] i_production_id The id of the production to create

	   @param [in] i_args Arguments for constructing the production.

	   @returns An optional object holding the constructed production instance
	   or nothing if there is no production with the provided production id.
	*/
	template < class Id_, class... Args_ >
	OptionalProduction Create (Id_&& i_production_id, Args_&&... i_args)
	{
		static_assert (
		    ::std::is_same<
		        typename FactoryFunctionPolicy::ReturnType,
		        decltype (::std::declval< FactoryFunction > () (
		            ::std::declval< Args_ > ()...)) >::value,
		    "Production object cannot be constructed from provided arguments.");

		return CreateImpl< kUseGlobalRegistry, kUseLocalRegistry > (
		    ::std::forward< Id_ > (i_production_id),
		    ::std::forward< Args_ > (i_args)...);
	}


	/**
	   @brief Registers a factory function with the given id if there is no
	   other factory function with the same id have already been registered

	   @tparam Id_ Type of the id of the function to register

	   @tparam Function_ Type of the factory function to register

	   @param [in] i_production_id The id of the factory function to register

	   @param [in] i_factory_function The factory function to register

	   @returns "true" if the factory function was successfully registered,
	   "false" otherwise
	*/
	template < class Id_, class Function_ >
	bool RegisterProduction (
	    Id_&& i_production_id, Function_&& i_factory_function)
	{
		static_assert (
		    kUseLocalRegistry,
		    "Local registry is disabled. Use "
		    "Factory::Global::RegisterProduction() instead.");

		return RegisterProductionImpl< kUseLocalRegistry > (
		    ::std::forward< Id_ > (i_production_id),
		    ::std::forward< Function_ > (i_factory_function));
	}


	/**
	   @brief Registers a factory function with the given id if there is no
	   other factory function with the same id have already been registered or
	   replaces the previously registered factory function

	   @tparam Id_ Type of the id of the function to register

	   @tparam Function_ Type of the factory function to register

	   @param [in] i_production_id The id of the factory function to register

	   @param [in] i_factory_function The factory function to register
	*/
	template < class Id_, class Function_ >
	void RegisterOrReplaceProduction (
	    Id_&& i_production_id, Function_&& i_factory_function)
	{
		static_assert (
		    kUseLocalRegistry,
		    "Local registry is disabled. Use "
		    "Factory::Global::RegisterOrReplaceProduction() instead.");

		RegisterOrReplaceProductionImpl< kUseLocalRegistry > (
		    ::std::forward< Id_ > (i_production_id),
		    ::std::forward< Function_ > (i_factory_function));
	};


	/**
	   @brief Removes an object with the given id from the registry

	   @tparam Key_ Type of the id of the object to remove from the registry

	   @param [in] i_object_id The id of the object to remove from the registry
	*/
	template < class Id_ >
	void UnregisterProduction (Id_&& i_production_id)
	{
		static_assert (
		    kUseLocalRegistry,
		    "Local registry is disabled. Use "
		    "Factory::Global::UnregisterProduction() instead.");

		UnregisterProductionImpl< kUseLocalRegistry > (
		    ::std::forward< Id_ > (i_production_id));
	};


private:
	template < bool kUseGlobalRegistry_, class >
	class GlobalContext {
	public:
		template < class... AddObjectArgs_ >
		constexpr static bool RegisterProduction (AddObjectArgs_&&... i_args)
		{
			static_assert (
			    kUseGlobalRegistry_, "Global production registry is disabled.");

			return false;
		}

		template < class... AddOrReplaceObjectArgs_ >
		constexpr static void
		    RegisterOrReplaceProduction (AddOrReplaceObjectArgs_&&... i_args)
		{
			static_assert (
			    kUseGlobalRegistry_, "Global production registry is disabled.");
		}

		template < class... RemoveObjectArgs_ >
		constexpr static void
		    UnregisterProduction (RemoveObjectArgs_&&... i_args)
		{
			static_assert (
			    kUseGlobalRegistry_, "Global production registry is disabled.");
		}
	};


	template < bool kUseGlobal_, bool kUseLocal_, class Id_, class... Args_ >
	auto CreateImpl (Id_&& i_production_id, Args_&&... i_args)
	    -> ::std::enable_if_t< kUseGlobal_ && kUseLocal_, OptionalProduction >;

	template < bool kUseGlobal_, bool kUseLocal_, class Id_, class... Args_ >
	auto CreateImpl (Id_&& i_production_id, Args_&&... i_args)
	    -> ::std::enable_if_t< !kUseGlobal_ && kUseLocal_, OptionalProduction >;

	template < bool kUseGlobal_, bool kUseLocal_, class Id_, class... Args_ >
	auto CreateImpl (Id_&& i_production_id, Args_&&... i_args)
	    -> ::std::enable_if_t< kUseGlobal_ && !kUseLocal_, OptionalProduction >;

	template < bool kUseLocal_, class Id_, class Function_ >
	auto RegisterProductionImpl (
	    Id_&& i_production_id, Function_&& i_factory_function)
	    -> ::std::enable_if_t< kUseLocal_, bool >;

	template < bool kUseLocal_, class Id_, class Function_ >
	auto RegisterOrReplaceProductionImpl (
	    Id_&& i_production_id, Function_&& i_factory_function)
	    -> ::std::enable_if_t< kUseLocal_, void >;

	template < bool kUseLocal_, class Id_ >
	auto UnregisterProductionImpl (Id_&& i_production_id)
	    -> ::std::enable_if_t< kUseLocal_, void >;


	LocalFactoryRegistry registry_;
};


UTK_FACTORY_V2_NAMESPACE_CLOSE


#endif /* ASCLEPIA_INCLUDE_UTK_FACTORY_V2_FACTORY_DECL_HPP */
