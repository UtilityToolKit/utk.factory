// Copyright 2016-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.factory/utk.factory/include/utk/factory/v1/FactoryHelperMacros.hpp
//
// Description: Definitions of the helper macros to ease using the library.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V1_FACTORYHELPERMACROS_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V1_FACTORYHELPERMACROS_HPP


#include "utk/factory/v1/Factory.hpp"


/**
   @addtogroup Factory
   @{
*/

/**
  @def FACTORY_PRODUCTION_CREATE_OBJECT

   @brief Macro to simplify createObject function implementation.

   @details Defines a static method for object creation.

   @note Only works with simple createObject() methods with no arguments.

   @param [in] INTERFACE Class used for Factory class instantiation.

   @param [in] PRODUCTION Class inherited from INTERFACE.
*/
#define UTK_FACTORY_PRODUCTION_CREATE_OBJECT(INTERFACE, PRODUCTION)            \
	std::shared_ptr< INTERFACE > PRODUCTION::createObject ()                   \
	{                                                                          \
		return std::shared_ptr< INTERFACE > (new PRODUCTION);                  \
	}


/**
   @def UTK_FACTORY_PRODUCTION_TYPEID

   @brief Macro to simplify typeId function implementation.

   @details Defines a static method that returns PRODUCTION class typed ID equal
   to TYPEID.

   @param [in] PRODUCTION Class for which typeId() method will be defined.

   @param [in] TYPEID Desired PRODUCTION class type ID value.
*/
#define UTK_FACTORY_PRODUCTION_TYPEID(PRODUCTION, TYPEID)                      \
	StringType PRODUCTION::typeId ()                                           \
	{                                                                          \
		static StringType s_typeId (TYPEID);                                   \
                                                                               \
		return s_typeId;                                                       \
	}


#ifdef UTK_FACTORY_USE_QT
/**
   @def UTK_FACTORY_PRODUCTION_TYPEID_QT_METATYPE

   @brief Macro to simplify typeId function implementation for classes that use
   QMetaType::typeName() as type ID.

   @details Defines a static method that returns PRODUCTION class typed ID equal
   to QMetaType::typeName() of METATYPE.

   @param [in] PRODUCTION Class for which typeId() method will be defined.

   @param [in] METATYPE Class which QMetaType::typeName() will be used as
   PRODUCTION type ID.
*/
#	define UTK_FACTORY_PRODUCTION_TYPEID_QT_METATYPE(PRODUCTION, METATYPE)    \
		UTK_FACTORY_PRODUCTION_TYPEID (                                        \
		    PRODUCTION, QMetaType::typeName (qMetaTypeId< METATYPE > ()))
#endif

/**
   @}
*/


#endif /* UTK_FACTORY_INCLUDE_UTK_FACTORY_V1_FACTORYHELPERMACROS_HPP */
