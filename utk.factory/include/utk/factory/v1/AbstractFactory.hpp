// Copyright 2016-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.factory/utk.factory/include/utk/factory/v1/AbstractFactory.hpp
//
// Description: Abstract factory class declaration.


#ifndef UTK_FACTORY_INCLUDE_UTK_FACTORY_V1_ABSTRACTFACTORY_HPP
#define UTK_FACTORY_INCLUDE_UTK_FACTORY_V1_ABSTRACTFACTORY_HPP


#include "utk/factory/v1/Factory.hpp"


/**
   @addtogpoup Factory
   @{
*/

namespace utk {
	namespace factory {
		inline namespace v1 {
			/**
			   @brief Abstract fastory class able to produce objects of any of
			   registered classes.

			   @details May be used if someone would like not to use lots of
			   different factories or when minimization of number of includes is
			   desired.
			*/
			class AbstractFactory {
			public:
#ifdef UTK_FACTORY_USE_VARIADIC_TEMPLATES
				template < class Interface, class... Arguments >
				static std::shared_ptr< Interface >
				    createObject (QString i_type_id, Arguments... io_args)
				{
					return Factory< Interface >::createObject (
					    i_type_id, io_args...);
				}
#else
				template < class Interface >
				static std::shared_ptr< Interface >
				    createObject (QString i_type_id)
				{
					return Factory< Interface >::createObject (i_type_id);
				}
#endif
			};
		} // namespace v1
	}     // namespace factory
} // namespace utk

/**
   @}
*/


#endif /* UTK_FACTORY_INCLUDE_UTK_FACTORY_V1_ABSTRACTFACTORY_HPP */
