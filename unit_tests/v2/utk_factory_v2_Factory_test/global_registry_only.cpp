#include "includes.hpp"
#include "test_types.hpp"


TEMPLATE_PRODUCT_TEST_CASE (
    "TestProduction Factory with only global registry",
    "[global]",
    ::utk::Factory,
    ((int,
      ::utk::StdFactoryFunctionPolicy< TestProductionCreateValue >,
      ::utk::NoLocalRegistryPolicy,
      ::utk::GlobalRegistryPolicy<
          TestGlobalRegistry,
          TestGlobalRegistryProvider,
          false >,
      ::utk::BoostOptionalPolicy),
     (int,
      ::utk::StdFactoryFunctionPolicy< TestProductionCreateValue >,
      ::utk::NoLocalRegistryPolicy,
      ::utk::GlobalRegistryPolicy<
          TestGlobalRegistry,
          TestGlobalRegistryProvider,
          true >,
      ::utk::BoostOptionalPolicy),
     (int,
      ::utk::StdFactoryFunctionPolicy< TestProductionCreateValue >,
      ::utk::NoLocalRegistryPolicy,
      ::utk::GlobalRegistryPolicy<
          TestGlobalRegistry,
          TestGlobalRegistryProvider,
          false >,
      ::utk::SmartPointerAsOptionalPolicy<
          ::utk::StdReferenceWrapperPolicy,
          ::utk::StdSmartPointerPolicy< StdUniquePtr > >),
     (int,
      ::utk::StdFactoryFunctionPolicy<
          TestProductionCreateBoostOptional,
          TestProduction,
          ::utk::BoostOptionalPolicy >,
      ::utk::NoLocalRegistryPolicy,
      ::utk::GlobalRegistryPolicy<
          TestGlobalRegistry,
          TestGlobalRegistryProvider,
          false >),
     (int,
      ::utk::StdFactoryFunctionPolicy<
          TestProductionCreateStdUniquePtr,
          TestProduction,
          ::utk::SmartPointerAsOptionalPolicy<
              ::utk::StdReferenceWrapperPolicy,
              ::utk::StdSmartPointerPolicy< StdUniquePtr > > >,
      ::utk::NoLocalRegistryPolicy,
      ::utk::GlobalRegistryPolicy<
          TestGlobalRegistry,
          TestGlobalRegistryProvider,
          false >)))
{
	GIVEN ("Factory that was just initialised")
	{
		TestType factory;

		auto global_registry_raii = TestGlobalRegistryProvider<
		    typename TestType::GlobalFactoryRegistry >::GlobalRegistryRaii{};

		using OptionalPolicy = typename TestType::OptionalPolicy;
		using GlobalRegistryProvider =
		    typename TestType::GlobalRegistryProvider;

		using FactoryFunctionPolicy = typename TestType::FactoryFunctionPolicy;

		using FactoryFunctionFactory =
		    TestUserFactoryFunctionFactory< FactoryFunctionPolicy >;

		WHEN ("the factory is just initialised")
		{
			THEN (
			    "the factory can produce objects with identifiers from 1 to 3")
			{
				auto obj_1_optional = factory.Create (1, 1, "1");
				auto obj_2_optional = factory.Create (2, 2, "2");
				auto obj_3_optional = factory.Create (3, 3, "3");

				REQUIRE (OptionalPolicy::HasValue (obj_1_optional));
				REQUIRE (OptionalPolicy::HasValue (obj_2_optional));
				REQUIRE (OptionalPolicy::HasValue (obj_3_optional));

				AND_THEN (
				    "contents of the created objects match the provided "
				    "arguments")
				{
					auto const& obj_1 = OptionalPolicy::Get (obj_1_optional);
					auto const& obj_2 = OptionalPolicy::Get (obj_2_optional);
					auto const& obj_3 = OptionalPolicy::Get (obj_3_optional);

					REQUIRE (((obj_1.i == 1) && (obj_1.s == "1")));
					REQUIRE (((obj_2.i == 2) && (obj_2.s == "2")));
					REQUIRE (((obj_3.i == 3) && (obj_3.s == "3")));
				}
			}
		}

		WHEN ("trying to add new production with a duplicate key")
		{
			THEN ("operation fails returning \"false\"")
			{
				REQUIRE (!TestType::Global::RegisterProduction (
				    1,
				    FactoryFunctionPolicy::CreateDefaultFactoryFunction<> ()));
			}
		}

		WHEN ("trying to add new production with a unique key")
		{
			THEN ("operation succeeds returning \"true\"")
			{
				REQUIRE (TestType::Global::RegisterProduction (
				    4, FactoryFunctionFactory::CreateFactoryFunction ()));

				AND_THEN (
				    "trying to create an object with the same key results in "
				    "object created by the previously provided factory "
				    "function")
				{
					auto obj_4_optional = factory.Create (4, 4, "4");

					auto const& obj_4 = OptionalPolicy::Get (obj_4_optional);

					REQUIRE (
					    (OptionalPolicy::HasValue (obj_4_optional) &&
					     obj_4.creation_site ==
					         TestProduction::CreationSite::kUser));
				}
			}
		}

		WHEN (
		    "trying to add new or replace existing production with a duplicate "
		    "key")
		{
			TestType::Global::RegisterOrReplaceProduction (
			    1, FactoryFunctionFactory::CreateFactoryFunction ());

			THEN (
			    "trying to create an object with the same key results in "
			    "object created by the previously provided factory function")
			{
				auto obj_1_optional = factory.Create (1, 1, "1");

				REQUIRE (
				    (TestType::kPreferGlobalRegistry ||
				     (OptionalPolicy::HasValue (obj_1_optional) &&
				      OptionalPolicy::Get (obj_1_optional).creation_site ==
				          TestProduction::CreationSite::kUser)));
			}
		}

		WHEN ("trying to unregister production with some key")
		{
			TestType::Global::UnregisterProduction (1);

			auto obj_1_optional = factory.Create (1, 1, "1");

			THEN (
			    "trying to create removed production results in an empty "
			    "optional object")
			{
				REQUIRE (!OptionalPolicy::HasValue (obj_1_optional));
			}
		}
	}
}
