project (utk_factory_v2_Factory_test)


add_executable (${PROJECT_NAME}
  basic_usage.cpp
  global_registry_only.cpp
  includes.hpp
  main.cpp
  test_types.hpp
  )


set_target_properties (
  ${PROJECT_NAME}
  PROPERTIES
  CXX_STANDARD           ${utk_factory_V2_UNIT_TESTS_CXX_STANDARD}
  CXX_STANDARD_REQUIRED  TRUE
  FOLDER                 "${PROJECT_FOLDER}/Tests"
  )


target_link_libraries (${PROJECT_NAME}
  # Tested library
  utk::factory
  # Testing framework
  CONAN_PKG::boost_container
  CONAN_PKG::boost_optional
  CONAN_PKG::Catch2
  )


if (INSTALL_TESTS)
  include (GNUInstallDirs)

  install (TARGETS
    ${PROJECT_NAME}
    RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}")
endif ()


add_test (NAME ${PROJECT_NAME}
  COMMAND ${PROJECT_NAME})
