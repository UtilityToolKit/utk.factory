#ifndef UNIT_TESTS_V2_UTK_FACTORY_V2_FACTORY_TEST_TEST_TYPES_HPP
#define UNIT_TESTS_V2_UTK_FACTORY_V2_FACTORY_TEST_TEST_TYPES_HPP


#include "includes.hpp"


struct TestProduction {
	enum class CreationSite { kLocal, kGlobal, kUser, kUndefined };


	TestProduction (
	    int i_i,
	    ::std::string i_s,
	    CreationSite i_creation_site = CreationSite::kUndefined)
	    : i{i_i}
	    , s{std::move (i_s)}
	    , creation_site{i_creation_site}
	{
	}


	int i;
	::std::string s;

	CreationSite creation_site;
};


template <
    class FactoryFunctionPolicy_,
    TestProduction::CreationSite CreationSite_ >
class TestFactoryFunctionFactory {
public:
	using FactoryFunctionPolicy = FactoryFunctionPolicy_;
	using FactoryFunction = typename FactoryFunctionPolicy::FactoryFunction;
	using ProductionReturnType = typename FactoryFunctionPolicy::ReturnType;
	using Production = typename FactoryFunctionPolicy::Production;
	static auto constexpr CreationSite = CreationSite_;


	static auto CreateFactoryFunction ()
	{
		return CreateFactoryFunctionImpl<
		    ProductionReturnType,
		    FactoryFunctionPolicy::kReturnsOptional > ();
	}


private:
	template < class ReturnType_, bool kReturnsOptional_ >
	static auto CreateFactoryFunctionImpl ()
	    -> ::std::enable_if_t< !kReturnsOptional_, FactoryFunction >
	{
		return FactoryFunctionPolicy::CreateFactoryFunction (
		    [](int i_i, ::std::string i_s) {
			    return ReturnType_{i_i, ::std::move (i_s), CreationSite};
		    });
	}


	template < class ReturnType_, bool kReturnsOptional_ >
	static auto CreateFactoryFunctionImpl ()
	    -> ::std::enable_if_t< kReturnsOptional_, FactoryFunction >
	{
		return FactoryFunctionPolicy::CreateFactoryFunction (
		    [](int i_i, ::std::string i_s) {
			    return FactoryFunctionPolicy::OptionalPolicy::MakeOptional<
			        Production > (i_i, ::std::move (i_s), CreationSite);
		    });
	}
};


template < class ContainerPolicy_, TestProduction::CreationSite CreationSite_ >
class TestContainerInitializer {
public:
	using ContainerPolicy = ContainerPolicy_;
	static auto constexpr CreationSite = CreationSite_;


	template < class Container_ >
	static void InitContainer (Container_&& io_container)
	{
		using FactoryFunction =
		    typename ContainerPolicy::template Value< Container_ >;

		using ProductionReturnType = typename FactoryFunction::result_type;

		for (int i = 1; i < 4; ++i)
		{
			ContainerPolicy::Emplace (
			    ::std::forward< Container_ > (io_container),
			    i,
			    CreateFactoryFunction<
			        FactoryFunction,
			        ProductionReturnType > ());
		}
	}


private:
	template < class FactoryFunction_, class ReturnType_ >
	static auto CreateFactoryFunction () -> ::std::enable_if_t<
	    ::std::is_constructible< ReturnType_, TestProduction&& >::value,
	    FactoryFunction_ >
	{
		return [](int i_i, ::std::string i_s) {
			return TestProduction{i_i, ::std::move (i_s), CreationSite};
		};
	}


	template < class FactoryFunction_, class ReturnType_ >
	static auto CreateFactoryFunction () -> ::std::enable_if_t<
	    ::std::is_constructible< ReturnType_, TestProduction* >::value,
	    FactoryFunction_ >
	{
		return [](int i_i, ::std::string i_s) {
			return ReturnType_{
			    new TestProduction{i_i, ::std::move (i_s), CreationSite}};
		};
	}
};


template < class Registry_ >
struct TestGlobalRegistryProvider {
	using Registry = Registry_;

	constexpr static bool kCanPotentiallyProvide = true;


	struct GlobalRegistryRaii {
		GlobalRegistryRaii ()
		{
			TestGlobalRegistryProvider::SetGlobalRegistry (Registry{});
		}


		~GlobalRegistryRaii ()
		{
			TestGlobalRegistryProvider::ResetGlobalRegistry ();
		}
	};


	template < class NewRegistry_ >
	static void SetGlobalRegistry (NewRegistry_&& i_new_registry)
	{
		static_assert (
		    ::std::is_same< Registry, NewRegistry_ >::value,
		    "Incompatible registry type");

		kGlobalRegistry = ::std::forward< NewRegistry_ > (i_new_registry);
	}


	static void ResetGlobalRegistry ()
	{
		kGlobalRegistry = ::boost::none;
	}


	static Registry_& GlobalRegistry ()
	{
		return *kGlobalRegistry;
	}


	static bool HaveGlobalRegistry ()
	{
		return bool(kGlobalRegistry);
	}

	static ::boost::optional< Registry_ > kGlobalRegistry;
};


template < class Key_, class Value_ >
using StdMap = ::std::map< Key_, Value_ >;


template < class Registry_ >
::boost::optional< Registry_ >
    TestGlobalRegistryProvider< Registry_ >::kGlobalRegistry = ::boost::none;


template <
    class ObjectId_,
    class Object_,
    template < class Container_ >
    class ContainerInitializer_ >
using TestRegistry = ::utk::ObjectRegistry<
    ObjectId_,
    Object_,
    ::utk::BoostOptionalPolicy,
    ::utk::StdMapCompatibleContainerPolicy< StdMap, ContainerInitializer_ > >;


template < class Container_ >
using TestLocalContainerInitializer = TestContainerInitializer<
    Container_,
    TestProduction::CreationSite::kLocal >;


template < class Container_ >
using TestGlobalContainerInitializer = TestContainerInitializer<
    Container_,
    TestProduction::CreationSite::kGlobal >;


template < class ObjectId_, class Object_ >
using TestLocalRegistry =
    TestRegistry< ObjectId_, Object_, TestLocalContainerInitializer >;


template < class ObjectId_, class Object_ >
using TestGlobalRegistry =
    TestRegistry< ObjectId_, Object_, TestGlobalContainerInitializer >;


template < class FactoryFunctionPolicy_ >
using TestUserFactoryFunctionFactory = TestFactoryFunctionFactory<
    FactoryFunctionPolicy_,
    TestProduction::CreationSite::kUser >;


template < class T_ >
using StdUniquePtr = ::std::unique_ptr< T_ >;


using TestProductionCreateValue =
    ::std::function< TestProduction (int, ::std::string) >;


using TestProductionCreateBoostOptional =
    ::std::function< ::boost::optional< TestProduction > (int, ::std::string) >;


using TestProductionCreateStdUniquePtr =
    ::std::function< StdUniquePtr< TestProduction > (int, ::std::string) >;


#endif /* UNIT_TESTS_V2_UTK_FACTORY_V2_FACTORY_TEST_TEST_TYPES_HPP */
