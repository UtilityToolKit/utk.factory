#ifndef UNIT_TESTS_V2_UTK_FACTORY_V2_FACTORY_TEST_INCLUDES_HPP
#define UNIT_TESTS_V2_UTK_FACTORY_V2_FACTORY_TEST_INCLUDES_HPP


#include <map>
#include <memory>
#include <string>

#include <catch2/catch.hpp>

#include <utk/factory/v2/Factory_Decl.hpp>
#include <utk/factory/v2/Factory_Impl.hpp>

#include <utk/factory/v2/factory_function_policy/StdFactoryFunctionPolicy.hpp>

#include <utk/factory/v2/global_registry_policy/GlobalRegistryPolicy.hpp>
#include <utk/factory/v2/global_registry_policy/NoGlobalRegistryPolicy.hpp>
#include <utk/factory/v2/global_registry_policy/NoGlobalRegistryProvider.hpp>

#include <utk/factory/v2/local_registry_policy/LocalRegistryPolicy.hpp>
#include <utk/factory/v2/local_registry_policy/NoLocalRegistryPolicy.hpp>

#include <utk/factory/v2/container_policy/StdMapCompatibleContainerPolicy.hpp>

#include <utk/factory/v2/optional_policy/BoostOptionalPolicy.hpp>
#include <utk/factory/v2/optional_policy/SmartPointerAsOptionalPolicy.hpp>

#include <utk/factory/v2/reference_policy/StdReferenceWrapperPolicy.hpp>

#include <utk/factory/v2/smart_pointer_policy/StdSmartPointerPolicy.hpp>


#endif /* UNIT_TESTS_V2_UTK_FACTORY_V2_FACTORY_TEST_INCLUDES_HPP */
