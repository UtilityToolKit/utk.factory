#include <map>
#include <memory>
#include <string>

#include <boost/container/flat_map.hpp>
#include <boost/container/map.hpp>

#include <catch2/catch.hpp>

#include <utk/factory/v2/ObjectRegistry_Decl.hpp>
#include <utk/factory/v2/ObjectRegistry_Impl.hpp>

#include <utk/factory/v2/container_policy/StdMapCompatibleContainerPolicy.hpp>

#include <utk/factory/v2/optional_policy/BoostOptionalPolicy.hpp>
#include <utk/factory/v2/optional_policy/SmartPointerAsOptionalPolicy.hpp>

#include <utk/factory/v2/reference_policy/StdReferenceWrapperPolicy.hpp>

#include <utk/factory/v2/smart_pointer_policy/StdSmartPointerPolicy.hpp>


template < class ContainerPolicy_ >
struct TestContainerInitializer {
	using ContainerPolicy = ContainerPolicy_;

	template < class Container_ >
	static void InitContainer (Container_&& io_container)
	{
		for (int i = 1; i < 4; ++i)
		{
			ContainerPolicy::Emplace (
			    ::std::forward< Container_ > (io_container),
			    i,
			    ::std::to_string (i));
		}
	}
};


template < class T_ >
using StdUniquePtr = ::std::unique_ptr< T_ >;


template < class Key_, class Value_ >
using StdMap = ::std::map< Key_, Value_ >;

template < class Key_, class Value_ >
using StdMapTransparentComaprison = ::std::map< Key_, Value_, ::std::less<> >;

template < class Key_, class Value_ >
using BoostMap = ::boost::container::map< Key_, Value_ >;

template < class Key_, class Value_ >
using BoostFlatMap = ::boost::container::flat_map< Key_, Value_ >;


TEMPLATE_PRODUCT_TEST_CASE (
    "ObjectRegistry of (int, std::string) with TestContainerInitializer",
    "",
    ::utk::ObjectRegistry,
    ((int,
      ::std::string,
      ::utk::BoostOptionalPolicy,
      ::utk::
          StdMapCompatibleContainerPolicy< StdMap, TestContainerInitializer >),
     (int,
      ::std::string,
      ::utk::BoostOptionalPolicy,
      ::utk::StdMapCompatibleContainerPolicy<
          StdMapTransparentComaprison,
          TestContainerInitializer >),
     (int,
      ::std::string,
      ::utk::BoostOptionalPolicy,
      ::utk::StdMapCompatibleContainerPolicy<
          BoostMap,
          TestContainerInitializer >),
     (int,
      ::std::string,
      ::utk::BoostOptionalPolicy,
      ::utk::StdMapCompatibleContainerPolicy<
          BoostFlatMap,
          TestContainerInitializer >),
     (int,
      ::std::string,
      ::utk::SmartPointerAsOptionalPolicy<
          ::utk::StdReferenceWrapperPolicy,
          ::utk::StdSmartPointerPolicy< StdUniquePtr > >,
      ::utk::
          StdMapCompatibleContainerPolicy< StdMap, TestContainerInitializer >),
     (int,
      ::std::string,
      ::utk::SmartPointerAsOptionalPolicy<
          ::utk::StdReferenceWrapperPolicy,
          ::utk::StdSmartPointerPolicy< ::std::shared_ptr > >,
      ::utk::
          StdMapCompatibleContainerPolicy< StdMap, TestContainerInitializer >)))
{
	GIVEN ("ObjectRegistry that was just initialised")
	{
		TestType registry;

		using OptionalPolicy = typename TestType::OptionalPolicy;

		WHEN ("the registry is just initialised")
		{
			THEN (
			    "the registry contains pairs (1, \"1\"), (2, \"2\"), (3, "
			    "\"3\"))")
			{
				REQUIRE (
				    (OptionalPolicy::HasValue (registry.GetObject (1)) &&
				     OptionalPolicy::Get (registry.GetObject (1)) ==
				         ::std::string{"1"}));
				REQUIRE (
				    (OptionalPolicy::HasValue (registry.GetObject (2)) &&
				     OptionalPolicy::Get (registry.GetObject (2)) ==
				         ::std::string{"2"}));
				REQUIRE (
				    (OptionalPolicy::HasValue (registry.GetObject (3)) &&
				     OptionalPolicy::Get (registry.GetObject (3)) ==
				         ::std::string{"3"}));
			}
		}

		WHEN ("getting two copies of the object with the same key")
		{
			auto obj_1 = registry.GetObject (1);
			auto obj_2 = registry.GetObject (1);

			THEN ("both objects are not null")
			{
				REQUIRE (OptionalPolicy::HasValue (obj_1));
				REQUIRE (OptionalPolicy::HasValue (obj_2));

				AND_THEN (
				    "getting address of the referenced objects gives the "
				    "different results")
				{
					REQUIRE (
					    &OptionalPolicy::Get (obj_1) !=
					    &OptionalPolicy::Get (obj_2));
				}
			}
		}

		WHEN ("getting two references to the object with some key")
		{
			auto ref_1 = registry.GetObjectRef (1);
			auto ref_2 = registry.GetObjectRef (1);

			THEN ("both reference objects are not null")
			{
				REQUIRE (OptionalPolicy::HasValue (ref_1));
				REQUIRE (OptionalPolicy::HasValue (ref_2));

				AND_THEN (
				    "getting address of the referenced objects gives the same "
				    "result")
				{
					REQUIRE (
					    &OptionalPolicy::Get (ref_1) ==
					    &OptionalPolicy::Get (ref_2));
				}
			}
		}

		WHEN ("getting two const references to the object with some key")
		{
			auto ref_1 = registry.GetObjectConstRef (1);
			auto ref_2 = registry.GetObjectConstRef (1);

			THEN ("both reference objects are not null")
			{
				REQUIRE (OptionalPolicy::HasValue (ref_1));
				REQUIRE (OptionalPolicy::HasValue (ref_2));

				AND_THEN (
				    "getting address of the referenced objects gives the same "
				    "result")
				{
					REQUIRE (
					    &OptionalPolicy::Get (ref_1) ==
					    &OptionalPolicy::Get (ref_2));
				}
			}
		}

		WHEN ("getting references to the objects with different keys")
		{
			auto ref_1 = registry.GetObjectRef (2);
			auto ref_2 = registry.GetObjectRef (3);

			THEN ("both reference objects are not null")
			{
				REQUIRE (OptionalPolicy::HasValue (ref_1));
				REQUIRE (OptionalPolicy::HasValue (ref_2));

				AND_THEN (
				    "getting address of the referenced objects gives different "
				    "results")
				{
					REQUIRE (
					    &OptionalPolicy::Get (ref_1) !=
					    &OptionalPolicy::Get (ref_2));

					AND_THEN ("the referenced values are not equal")
					{
						REQUIRE (
						    OptionalPolicy::Get (ref_1) !=
						    OptionalPolicy::Get (ref_2));
					}
				}
			}
		}

		WHEN ("getting const references to the objects with different keys")
		{
			auto ref_1 = registry.GetObjectConstRef (2);
			auto ref_2 = registry.GetObjectConstRef (3);

			THEN ("both reference objects are not null")
			{
				REQUIRE (OptionalPolicy::HasValue (ref_1));
				REQUIRE (OptionalPolicy::HasValue (ref_2));

				AND_THEN (
				    "getting address of the referenced objects gives different "
				    "results")
				{
					REQUIRE (
					    &OptionalPolicy::Get (ref_1) !=
					    &OptionalPolicy::Get (ref_2));

					AND_THEN ("the referenced values are not equal")
					{
						REQUIRE (
						    OptionalPolicy::Get (ref_1) !=
						    OptionalPolicy::Get (ref_2));
					}
				}
			}
		}

		WHEN ("trying to add an object with a duplicate key")
		{
			THEN ("operation fails returning \"false\"")
			{
				REQUIRE (!registry.AddObject (1, "fail"));
			}
		}

		WHEN ("trying to add an object with a unique key")
		{
			THEN ("operation succeeds returning \"true\"")
			{
				REQUIRE (registry.AddObject (4, "4"));

				AND_THEN (
				    "getting the object with the new key returns valid "
				    "optional with the added object")
				{
					REQUIRE (OptionalPolicy::HasValue (registry.GetObject (4)));
					REQUIRE (
					    OptionalPolicy::Get (registry.GetObject (4)) ==
					    ::std::string{"4"});
				}
			}
		}

		WHEN ("trying to add or replace an object with a duplicate key")
		{
			registry.AddOrReplaceObject (1, "!1");

			THEN (
			    "getting the object with the used key returns valid "
			    "optional with the new object")
			{
				REQUIRE (OptionalPolicy::HasValue (registry.GetObject (1)));
				REQUIRE (
				    OptionalPolicy::Get (registry.GetObject (1)) ==
				    ::std::string{"!1"});
			}
		}

		WHEN ("trying to add or replace an object with a unique key")
		{
			registry.AddOrReplaceObject (4, "4");

			THEN (
			    "getting the object with the new key returns valid "
			    "optional with the new object")
			{
				REQUIRE (OptionalPolicy::HasValue (registry.GetObject (4)));
				REQUIRE (
				    OptionalPolicy::Get (registry.GetObject (4)) ==
				    ::std::string{"4"});
			}
		}

		WHEN ("trying to remove object")
		{
			registry.RemoveObject (1);

			THEN ("trying to get the removed object results in empty optional")
			{
				REQUIRE (!OptionalPolicy::HasValue (registry.GetObject (1)));
			}
		}
	}
}
